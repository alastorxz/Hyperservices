<?php


class BaseDeDades {

    private $connexio;

      function __construct($servidor, $usuari, $contrasenya, $baseDades) {
        //Realitzem connexió
        $this->connexio = new mysqli($servidor, $usuari, $contrasenya, $baseDades);
      }


      function tancarConnexio() {
        $this->connexio->close();
      }

        //Alliberar memòria
      function alliberarMemoria($consulta){
        $consulta->free();
      }

      function consultar($sentenciSql) {
        return $this->connexio->query($sentenciSql);
      }

        //Obtenir registres d'una consulta
      function obtenirRegistres($consulta) {
        return $consulta->fetch_array(MYSQLI_ASSOC);
      }

}
?>