<?php

include_once "clases/BaseDeDades.php";

function mostrarProductos(){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT * FROM product");

    $infoProducts = array();
    while($registros = $conexio->obtenirRegistres($consulta)){
        array_push($infoProducts,array("idProduct"=>$registros["id"],"productName"=>$registros["productName"],"productType"=>$registros["productType"],"price"=>$registros["price"],"productDescription"=>$registros["productDescription"],"image"=>$registros["image"]));
    }

    return $infoProducts;
}

function mostrarInfoContratos(){
    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT * FROM infoContract");

    $infoContract = array();
    while($registros = $conexio->obtenirRegistres($consulta)){
        array_push($infoContract, array("Type"=>$registros["contractType"],"description"=>$registros["contractDescription"],"image"=>$registros["image"],"price"=>$registros["price"]));
    }

    return $infoContract;
}

function crearContrato($contrato, $fecha,$fechaFinal, $producto,$usuario, $vigencia){
    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT id FROM product WHERE productName = '".$producto."'");

    $idProduct;

    while($registros = $conexio->obtenirRegistres($consulta)){
        $idProduct = $registros["id"];
    }

    $consulta = $conexio->consultar("SELECT id FROM user WHERE nameUser ='".$usuario."'");

    $idUsuario;

    while($registros = $conexio->obtenirRegistres($consulta)){
        $idUsuario = $registros["id"];
    }

    if ($vigencia == 0){
    $consulta = $conexio->consultar("INSERT INTO contract (contractType,openedDate, closedDate,contractStatus,relatedProduct,relatedUser,vigency) VALUES ('".$contrato."','".$fecha."','".$fechaFinal."','Expired'".",'".$idProduct."','".$idUsuario."','".$vigencia."')");

    } else if ($vigencia == 1){
    $consulta = $conexio->consultar("INSERT INTO contract (contractType,openedDate,closedDate,contractStatus,relatedProduct,relatedUser,vigency) VALUES ('".$contrato."','".$fecha."','".$fechaFinal."','Opened'".",'".$idProduct."','".$idUsuario."','".$vigencia."')");


    } else if ($vigencia == 2) {
    $consulta = $conexio->consultar("INSERT INTO contract (contractType,openedDate,closedDate,contractStatus,relatedProduct,relatedUser,vigency) VALUES ('".$contrato."','".$fecha."','".$fechaFinal."','Opened'".",'".$idProduct."','".$idUsuario."','".$vigencia."')");


    }
    return true;
}

function mostrarContratosUsuario($usuario){
    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT id FROM user WHERE nameUser ='".$usuario."'");

    $idUsuario;

    while($registros = $conexio->obtenirRegistres($consulta)){
        $idUsuario = $registros["id"];
    }

   $consultaContract = $conexio->consultar("SELECT * FROM contract WHERE relatedUser = '".$idUsuario."'");

   $infoContratosUser = array();
   $idProduct;
   $nombreProducto;

   while($registros = $conexio->obtenirRegistres($consultaContract)){

        $idProduct = $registros["relatedProduct"];

        $consultaProduct = $conexio->consultar("SELECT * FROM product WHERE id = '".$idProduct."'");

        while($registrosProduct = $conexio->obtenirRegistres($consultaProduct)){
            $nombreProducto = $registrosProduct["productName"];
        }

        array_push($infoContratosUser, array("id"=>$registros["id"],"contractType"=>$registros["contractType"],"openedDate"=>$registros["openedDate"],"closedDate"=>$registros["closedDate"],"contractStatus"=>$registros["contractStatus"],"relatedProduct"=>$nombreProducto));
   }

   return $infoContratosUser;

}

function cambioEstadoExpired($id){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("UPDATE contract SET contractStatus = 'Expired' WHERE id = '".$id."'");

    return true;

}

function cambioEstadoAlmostExpired($id){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("UPDATE contract SET contractStatus = 'Almost Expired' WHERE id = '".$id."'");

    return true;

}

function mostrarContratosIncidencia($usuario){
    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT id FROM user WHERE nameUser ='".$usuario."'");

    while($registrosName = $conexio->obtenirRegistres($consulta)){
            $nameUser = $registrosName["id"];

    }

    $consultaContrato = $conexio->consultar("SELECT id FROM contract WHERE relatedUser = '".$nameUser."'");


    $contratos = array();

    while($registrosContract = $conexio->obtenirRegistres($consultaContrato)){
        array_push($contratos,array("id"=>$registrosContract["id"]));
    }

    return $contratos;
}

function crearCase($title, $description, $urgency, $impact, $contract){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("INSERT INTO cases (caseTitle, caseDescription, caseStatus, caseUrgency, caseImpact, relatedContract) VALUES ('".$title."','".$description."','Opened','".$urgency."','".$impact."','".$contract."')");
    return true;
}

function mostrarIncidenciasUsuario($usuario){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT id FROM user WHERE nameUser ='".$usuario."'");

    while($registrosName = $conexio->obtenirRegistres($consulta)){
                $nameUser = $registrosName["id"];
    }
    $consultaContrato = $conexio->consultar("SELECT id FROM contract WHERE relatedUser = '".$nameUser."'");

    $contratos = array();


    while($registrosContract = $conexio->obtenirRegistres($consultaContrato)){
        array_push($contratos,array("id"=>$registrosContract["id"]));
    }


    $cases = array();

    for ($i=0; $i<= count($contratos);$i++){
        $consultaCases = $conexio->consultar("SELECT * FROM cases WHERE relatedContract ='".$contratos[$i]["id"]."'");

        while($registrosCases = $conexio->obtenirRegistres($consultaCases)){
                array_push($cases,array("id"=>$registrosCases["id"],"caseTitle"=>$registrosCases["caseTitle"],"caseDescription"=>$registrosCases["caseDescription"],"caseStatus"=>$registrosCases["caseStatus"],"caseUrgency"=>$registrosCases["caseUrgency"],"caseImpact"=>$registrosCases["caseImpact"], "caseResolution"=>$registrosCases["caseResolution"], "relatedContract"=>$registrosCases["relatedContract"]));
            }

    }


    return $cases;

}

function sidebarUsuario($usuario){
 $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
 $consulta = $conexio->consultar("SELECT nameUser, firstName, lastName, image, mail FROM user WHERE nameUser ='".$usuario."'");

    $user = array();

     while($registro = $conexio->obtenirRegistres($consulta)){
        array_push($user,array("nameUser"=>$registro["nameUser"],"firstName"=>$registro["firstName"],"lastName"=>$registro["lastName"],"mail"=>$registro["mail"],"image"=>$registro["image"]));
     }




   return $user;


}


?>