<?php

include ("Clases/BaseDeDades.php");

function infoProductos(){

    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $connexio->consultar("SELECT * FROM product");

    $products = array();

   while($registros = $connexio->obtenirRegistres($consulta)){
          array_push($products,array("idProduct"=>$registros["id"],"productName"=>$registros["productName"],"productType"=>$registros["productType"],"price"=>$registros["price"],"productDescription"=>$registros["productDescription"],"image"=>$registros["image"]));
      }

    return $products;
}

function infoContratos(){
    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("SELECT * FROM infoContract");

    $infoContracts = array();
    while($registros = $conexio->obtenirRegistres($consulta)){
        array_push($infoContracts, array("Type"=>$registros["contractType"],"description"=>$registros["contractDescription"],"image"=>$registros["image"],"price"=>$registros["price"]));
    }

    return $infoContracts;
}


function ContractsAdmin(){

   $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

   $consultaContract = $conexio->consultar("SELECT * FROM contract ORDER BY contractType DESC");

   $infoContratosAdmin = array();


   while($registros = $conexio->obtenirRegistres($consultaContract)){


        array_push($infoContratosAdmin, array("id"=>$registros["id"],"contractType"=>$registros["contractType"],"openedDate"=>$registros["openedDate"],"closedDate"=>$registros["closedDate"],"contractStatus"=>$registros["contractStatus"],"relatedProduct"=>$registros["relatedProduct"],"relatedUser"=>$registros["relatedUser"],"vigency"=>$registros["vigency"]));
   }

   return $infoContratosAdmin;

}

function cambioEstadoAdminExpired($id){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("UPDATE contract SET contractStatus = 'Expired' WHERE id = '".$id."'");

    return true;

}

function cambioEstadoAdminAlmostExpired($id){

    $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consulta = $conexio->consultar("UPDATE contract SET contractStatus = 'Almost Expired' WHERE id = '".$id."'");

    return true;

}



function CasesAdmin(){

 $conexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

   $consultaCases = $conexio->consultar("SELECT * FROM cases");

   $infoCasesAdmin = array();


   while($registro = $conexio->obtenirRegistres($consultaCases)){
               array_push($infoCasesAdmin,array("id"=>$registro["id"],"caseTitle"=>$registro["caseTitle"],"caseDescription"=>$registro["caseDescription"],"caseStatus"=>$registro["caseStatus"],"caseUrgency"=>$registro["caseUrgency"],"caseImpact"=>$registro["caseImpact"], "caseResolution"=>$registro["caseResolution"], "relatedContract"=>$registro["relatedContract"]));
           }

   return $infoCasesAdmin;

}

function mostrarUsuarios(){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consultaUsuarios = $connexio->consultar("SELECT * FROM user");

    $infoUsers = array();

    while($registros = $connexio->obtenirRegistres($consultaUsuarios)){
        array_push($infoUsers, array("id"=>$registros["id"],"userType"=>$registros["userType"],"firstName"=>$registros["firstName"],"lastName"=>$registros["lastName"],"nameUser"=>$registros["nameUser"],"passwd"=>$registros["passwd"],"direction"=>$registros["direction"],"postalCode"=>$registros["postalCode"],"mail"=>$registros["mail"],"city"=>$registros["city"],"country"=>$registros["country"],"businessSector"=>$registros["businessSector"],"contactNumber"=>$registros["contactNumber"],"idiom"=>$registros["idiom"],"image"=>$registros["image"]));
    }

    return $infoUsers;
}

function infoUser($userId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consultaUsuario  =$connexio->consultar("SELECT * FROM user WHERE id ='".$userId."'");

    $infoUser = array();

    while($registros = $connexio->obtenirRegistres($consultaUsuario)){
        array_push($infoUser, array("id"=>$registros["id"],"userType"=>$registros["userType"],"firstName"=>$registros["firstName"],"lastName"=>$registros["lastName"],"nameUser"=>$registros["nameUser"],"passwd"=>$registros["passwd"],"direction"=>$registros["direction"],"postalCode"=>$registros["postalCode"],"mail"=>$registros["mail"],"city"=>$registros["city"],"country"=>$registros["country"],"businessSector"=>$registros["businessSector"],"contactNumber"=>$registros["contactNumber"],"idiom"=>$registros["idiom"],"image"=>$registros["image"]));
    }

    return $infoUser;
}

function generatePassword($userId){
    $guid = "";
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);

    for ($i = 0; $i < 10; $i++) {
        $guid .= $characters[rand(0, $charactersLength - 1)];
    }

    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $updatePassword = $connexio->consultar("UPDATE user SET passwd = '".$guid."' WHERE id = '".$userId."'");
    return true;
}

function infoCase($caseId){

    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consultaCase = $connexio->consultar("SELECT * FROM cases WHERE id = '".$caseId."'");

    $infoCase = array();

    while($registros = $connexio->obtenirRegistres($consultaCase)){
        array_push($infoCase, array("id"=>$registros["id"],"caseTitle"=>$registros["caseTitle"],"caseDescription"=>$registros["caseDescription"],"caseStatus"=>$registros["caseStatus"],"caseUrgency"=>$registros["caseUrgency"],"caseImpact"=>$registros["caseImpact"],"caseResolution"=>$registros["caseResolution"],"relatedContract"=>$registros["relatedContract"]));
    }



    return $infoCase;

}

function infoRelatedUser($infoCase){
    $idContract = "";

    foreach($infoCase as $field){
        $idContract = $field["relatedContract"];
    }

    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $consultaRelatedUser = $connexio->consultar("SELECT relatedUser FROM contract WHERE id = '".$idContract."'");

    $idRelatedUser = "";

    while($registros = $connexio->obtenirRegistres($consultaRelatedUser)){
        $idRelatedUser = $registros["relatedUser"];
    }

    $consultaNameRelatedUser = $connexio->consultar("SELECT nameUser FROM user WHERE id = '".$idRelatedUser."'");

    $usernameRelatedUser = "";

    while($registros = $connexio->obtenirRegistres($consultaNameRelatedUser)){
        $usernameRelatedUser = $registros["nameUser"];
    }

    return $usernameRelatedUser;
}

function infoRelatedProduct($infoCase){
    $idContract = "";

    foreach($infoCase as $field){
        $idContract = $field["relatedContract"];
    }

    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $consultaRelatedUser = $connexio->consultar("SELECT relatedProduct FROM contract WHERE id = '".$idContract."'");

    $idRelatedProduct = "";

    while($registros = $connexio->obtenirRegistres($consultaRelatedUser)){
        $idRelatedProduct = $registros["relatedProduct"];
    }

    $consultaNameRelatedProduct = $connexio->consultar("SELECT productName FROM product WHERE id = '".$idRelatedProduct."'");

    $nameRelatedProduct = "";

    while($registros = $connexio->obtenirRegistres($consultaNameRelatedProduct)){
        $nameRelatedProduct = $registros["productName"];
    }

    return $nameRelatedProduct;
}

function updateCaseTitle($caseTitle, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseTitle = $connexio->consultar("UPDATE cases SET caseTitle = '".$caseTitle."' WHERE id = '".$caseId."'");
    return true;
}

function updateCaseStatus($caseStatus, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseStatus = $connexio->consultar("UPDATE cases SET caseStatus = '".$caseStatus."' WHERE id = '".$caseId."'");
    return true;
}

function updateCaseDescription($caseDescription, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseDescription = $connexio->consultar("UPDATE cases SET caseDescription = '".$caseDescription."' WHERE id = '".$caseId."'");
    return true;
}

function updateCaseUrgency($caseUrgency, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseUrgency = $connexio->consultar("UPDATE cases SET caseUrgency = '".$caseUrgency."' WHERE id = '".$caseId."'");
    return true;
}

function updateCaseImpact($caseImpact, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseImpact = $connexio->consultar("UPDATE cases SET caseImpact = '".$caseImpact."' WHERE id = '".$caseId."'");
    return true;
}

function updateCaseResolution($caseResolution, $caseId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");

    $updateCaseResolution = $connexio->consultar("UPDATE cases SET caseResolution = '".$caseResolution."' WHERE id = '".$caseId."'");
    return true;
}

function userName($userId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $consultaNameUser = $connexio->consultar("SELECT nameUser FROM user WHERE id ='".$userId."'");

    $nameUser = "";

    while($registros = $connexio->obtenirRegistres($consultaNameUser)){
        $nameUser = $registros["nameUser"];
    }

    return $nameUser;
}

function subirFoto($url,$user){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    $sentencia = "UPDATE user SET image='".$url."' WHERE nameUser = '".$user."'";
    $connexio->consultar($sentencia);
}

function changeUserType($typeChecked, $userId){
    $connexio = new BaseDeDades("localhost:3306","root","Hyperservices","proyecto");
    if($typeChecked == "client"){
        $sentencia = "SELECT id FROM `contract` WHERE `relatedUser` ='".$userId."'";
        $consultaIDContratos = $connexio->consultar($sentencia);
        $contractIDs = array();

        while($registros = $connexio->obtenirRegistres($consultaIDContratos)){
            array_push($contractIDs, array("id"=>$registros["id"]));
        }

        foreach($contractIDs as $contractId){
            $sentencia = "DELETE FROM cases WHERE relatedContract = '".$contractId["id"]."'";
            $consultaIDCases = $connexio->consultar($sentencia);
        }

        $sentencia = "DELETE FROM `contract` WHERE `relatedUser`='".$userId."'";
        $connexio->consultar($sentencia);

        $sentencia = "UPDATE user SET userType= 'admin' WHERE id = '".$userId."'";
        $connexio->consultar($sentencia);

        return true;
    }else if($typeChecked == "admin"){
         $sentencia = "UPDATE user SET userType= 'client' WHERE id = '".$userId."'";
         $connexio->consultar($sentencia);

         return true;

    }

}

?>