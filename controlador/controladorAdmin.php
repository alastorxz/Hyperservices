<?php

include("../model/modelAdmin.php");

$dashboardAdmin = $_POST["dashboardAdmin"];
$dashboardContracts = $_POST["dashboardContractsAdmin"];
$contractsAdmin = $_POST["contractAdmin"];
$casesAdmin = $_POST["caseAdmin"];
$usersAdmin = $_POST["userAdmin"];

$logout = $_POST["logOut"];



if ($dashboardAdmin){

    $productos = infoProductos();
    include("../vista/viewAdmin/portalAdmin.php");


}

if ($dashboardContracts){

    $contratos = infoContratos();
    include ("../vista/viewAdmin/dashboardContractsAdmin.php");
}


if ($contractsAdmin){

    $allContracts = ContractsAdmin();

    foreach ($allContracts as $contrato){

        $datetime1 = new DateTime($contrato["openedDate"]);
        $datetime2 = new DateTime($contrato["closedDate"]);
        $fechahoy = date("Y-m-d");
        $datetime3 = new DateTime($fechahoy);
        $interval = $datetime3->diff($datetime2);
     


            if ($contrato["openedDate"] >= $contrato["closedDate"]){
                $update = cambioEstadoAdminExpired($contrato["id"]);
            }

            else if (($interval->format('%a') < 90) && ($interval->format('%a') > 0) ){
            $update = cambioEstadoAdminAlmostExpired($contrato["id"]);

            }

        }

        $allContracts = ContractsAdmin();

    include("../vista/viewAdmin/ContractsAdmin.php");

}



if($casesAdmin){
    $allCases = CasesAdmin();
    include("../vista/viewAdmin/casesAdmin.php");

}

if ($usersAdmin){
    session_start();
    $user = $_SESSION["user"];
    $infoUsers = mostrarUsuarios();
    include("../vista/viewAdmin/usersAdmin.php");

}

if ($logout){
session_destroy();
header("location:../vista/principal.php");

}
?>