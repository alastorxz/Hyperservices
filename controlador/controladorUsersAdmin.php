<?php
include("../model/modelAdmin.php");

$userId = $_POST["id"];
$changeUserType = $_POST["changeUserType"];
$changePasswd = $_POST["changePasswd"];
$fotoSubmit = $_POST["loadFoto"];

if($userId){
    session_start();
    $user = $_SESSION["user"];
    $infoUser = infoUser($userId);
    include ("../vista/viewAdmin/userDetail.php");
}

if($changeUserType){
    if($_POST["typeChecked"]){
        $typeChecked = $_POST["typeChecked"];
        $userId = $_POST["changeUserType"];
        $success = changeUserType($typeChecked, $userId);
        if($success){
            $infoUser = infoUser($userId);
            include ("../vista/viewAdmin/userDetail.php");
        }
    }
}

if($changePasswd){
    $userId = $_POST["changePasswd"];
    $success = generatePassword($userId);
    if($success){
        session_start();
        $user = $_SESSION["user"];
        $infoUser = infoUser($userId);
        include("../vista/viewAdmin/userDetail.php");
    }


}

if($fotoSubmit){
    $nom = $_FILES["fotoUser"]["name"];
    $tipus = $_FILES["fotoUser"]["type"];
    $extension=substr(strstr($tipus,"/"),1);
    $grandaria = $_FILES["fotoUser"]["size"];

    if ($grandaria > 2000000) {
        exit();
    }else{
        session_start();
        $userId = $_POST["userId"];
        $userName = userName($userId);
        if (move_uploaded_file($_FILES["fotoUser"]['tmp_name'], "../vista/images/fotosUser/".$userName.".".$extension)) {
           $url="".$userName.".".$extension;

           subirFoto($url,$userName);
        }
        $infoUser = infoUser($userId);
        include("../vista/viewAdmin/userDetail.php");
    }

}

?>