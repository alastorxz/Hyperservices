<?php

include("../model/modelAdmin.php");

$caseId = $_POST["caseId"];
$updateCaseTitle = $_POST["updateCaseTitle"];
$updateCaseStatus = $_POST["updateCaseStatus"];
$updateCaseDescription = $_POST["updateCaseDescription"];
$updateCaseUrgency = $_POST["updateCaseUrgency"];
$updateCaseImpact = $_POST["updateCaseImpact"];
$updateCaseResolution = $_POST["updateCaseResolution"];

if($caseId){

    $infoCase = infoCase($caseId);
    $infoRelatedUser = infoRelatedUser($infoCase);
    $infoRelatedProduct = infoRelatedProduct($infoCase);
    include("../vista/viewAdmin/casesDetail.php");

}

if($updateCaseTitle){
    $caseTitle = $_POST["caseTitle"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseTitle($caseTitle, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

if($updateCaseStatus){
    $caseStatus = $_POST["caseStatus"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseStatus($caseStatus, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

if($updateCaseDescription){
    $caseDescription = $_POST["caseDescription"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseDescription($caseDescription, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

if($updateCaseUrgency){
    $caseUrgency = $_POST["caseUrgency"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseUrgency($caseUrgency, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

if($updateCaseImpact){
    $caseImpact = $_POST["caseImpact"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseImpact($caseImpact, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

if($updateCaseResolution){
    $caseResolution = $_POST["caseResolution"];
    $caseId = $_POST["caseIdOcult"];

    $success = updateCaseResolution($caseResolution, $caseId);

    if($success){
        $infoCase = infoCase($caseId);
        $infoRelatedUser = infoRelatedUser($infoCase);
        $infoRelatedProduct = infoRelatedProduct($infoCase);
        include("../vista/viewAdmin/casesDetail.php");
    }
}

?>