<?php

include "../model/modelPrincipal.php";
include "../model/modelPortal.php";

$login = $_POST["login"];
$user = $_POST["username"];
$password = $_POST["password"];
$register = $_POST["register"];
$FAQ = $_POST["FAQ"];

$dashboard = $_POST["dashboard"];
$cases = $_POST["cases"];
$contracts  = $_POST["contracts"];
$profile = $_POST["profile"];

$newCase = $_POST["newCase"];
$newCaseSubmit = $_POST["createNewCase"];
$caseTitle = $_POST["caseTitle"];
$caseDescription = $_POST["caseDescription"];
$caseUrgency = $_POST["caseUrgency"];
$caseImpact = $_POST["caseImpact"];
$incidentContract = $_POST["incidentContract"];

$productsDashBoard = $_POST["products"];
$contractsDashBoard = $_POST["dashBoardContracts"];
$newContract = $_POST["newContract"];
$closeContractForm = $_POST["closeContractForm"];

$newContractSubmit = $_POST["createNewContract"];
$selectedContract = $_POST["contractSelected"];
$selectedProduct = $_POST["productSelected"];

$logout = $_POST["logOut"];

if ($login){
     
    if ($user != "" && $password != ""){
        $comprobar = comprobarUsuario($user,$password);

        if ($comprobar == true){
            
            $admin = comprobarAdmin($user);
            
            if($admin == true){
                session_start();    
                $_SESSION["user"] = $user;
                $productos = mostrarProductos();
                include("../vista/viewAdmin/portalAdmin.php");
            }else{
                session_start();    
                $_SESSION["user"] = $user;
                $usuario = sidebarUsuario($user);
                $productos = mostrarProductos();
                include("../vista/viewUser/portal.php");
                
            }
        }else{
            header("location:../vista/principal.php");
        }
    }else{
        header("location:../vista/principal.php");
    }
}

if($register){
    $success = true;
    include("../vista/registro.php");
}


if($dashboard){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $productos = mostrarProductos();
    include("../vista/viewUser/portal.php");
}

if($contracts){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $infoContratosUser = mostrarContratosUsuario($user);

    foreach ($infoContratosUser as $contrato){


    $datetime1 = new DateTime($contrato["openedDate"]);
    $datetime2 = new DateTime($contrato["closedDate"]);
    $fechahoy = date("Y-m-d");
            $datetime3 = new DateTime($fechahoy);
            $interval = $datetime3->diff($datetime2);





        if ($contrato["openedDate"] >= $contrato["closedDate"]){
            $update = cambioEstadoExpired($contrato["id"]);
        }

        else if (($interval->format('%a') < 90) && ($interval->format('%a') > 1) ){
        $update = cambioEstadoAlmostExpired($contrato["id"]);

        }

    }

    $infoContratosUser = mostrarContratosUsuario($user);

    include("../vista/viewUser/Contracts.php");
}

if($profile){
    include("../controlador/controladorPerfil.php");
}

if($productsDashBoard){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $productos = mostrarProductos();
    include("../vista/viewUser/portal.php");
}

if($contractsDashBoard){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $contratos = mostrarInfoContratos();
    include("../vista/viewUser/dashboardContracts.php");
}

if($newContract){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $contratos = mostrarInfoContratos();
    $productos = mostrarProductos();
    $infoContratosUser = mostrarContratosUsuario($user);
    $showForm = true;
    include("../vista/viewUser/Contracts.php");
}

if($closeContractForm){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $infoContratosUser = mostrarContratosUsuario($user);
    include("../vista/viewUser/Contracts.php");
}

if($newContractSubmit){
    if($selectedContract && $selectedProduct ){
        session_start();
        $openedDate = date("Y-m-d");


        $relatedUser = $_SESSION["user"];

        if ($selectedContract == "No Support"){
            $success =  crearContrato($selectedContract,$openedDate,$openedDate,$selectedProduct,$relatedUser, 0);
            if($success){
                $usuario = sidebarUsuario($relatedUser);
                $infoContratosUser = mostrarContratosUsuario($relatedUser);
                include("../vista/viewUser/Contracts.php");
            }

        }else if($selectedContract == "Support"){

        $año = substr($openedDate, 0, 4);
        $mes = substr($openedDate, 5, 2);
        $dia = substr($openedDate, 8);

        $año = (int)$año;
        $año = $año + 1;

        $fechaFinal = "".$año."-".$mes."-".$dia;

            $success =  crearContrato($selectedContract,$openedDate,$fechaFinal,$selectedProduct,$relatedUser, 1);
            if($success){
                $usuario = sidebarUsuario($relatedUser);
                $infoContratosUser = mostrarContratosUsuario($relatedUser);
                include("../vista/viewUser/Contracts.php");
            }


        }else if ($selectedContract == "VIP"){

        $año = substr($openedDate, 0, 4);
                $mes = substr($openedDate, 5, 2);
                $dia = substr($openedDate, 8);

                $año = (int)$año;
                $año = $año + 2;

                $fechaFinal = "".$año."-".$mes."-".$dia;

            $success =  crearContrato($selectedContract,$openedDate,$fechaFinal,$selectedProduct,$relatedUser, 2);
            if($success){
                $usuario = sidebarUsuario($relatedUser);
                $infoContratosUser = mostrarContratosUsuario($relatedUser);
                include("../vista/viewUser/Contracts.php");
            }

        }

    }

}

if($cases){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $mostrar = mostrarIncidenciasUsuario($user);
    include("../vista/viewUser/Cases.php");
}

if($newCase){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $incidentContract = mostrarContratosIncidencia($user);
    include("../vista/viewUser/newCase.php");

}

if ($newCaseSubmit){
    

    if ($caseImpact && $caseUrgency && $incidentContract){
        session_start();
        $create = crearCase($caseTitle,$caseDescription,$caseUrgency,$caseImpact,$incidentContract);
        $user = $_SESSION["user"];
        $usuario = sidebarUsuario($user);
        $mostrar = mostrarIncidenciasUsuario($user);

        include("../vista/viewUser/Cases.php");
    }

}

if ($FAQ){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    include("../vista/viewUser/FAQ.php");
}




if ($logout){
session_destroy();
header("location:../vista/principal.php");

}



?>
