<?php

include "../model/modelPerfil.php";

$fotoSubmit = $_POST["loadFoto"];

$submitBusinessSector = $_POST["submitBusinessSector"];
$submitPostalCode = $_POST["submitPostalCode"];
$submitPassword = $_POST["submitPassword"];
$submitIdiom = $_POST["submitIdiom"];
$submitCity = $_POST["submitCity"];
$submitCountry = $_POST["submitCountry"];
$submitEmail = $_POST["submitEmail"];
$submitPhone = $_POST["submitPhone"];
$submitDirection = $_POST["submitDirection"];

if($profile){
    session_start();
    $user = $_SESSION["user"];
    $usuario = sidebarUsuario($user);
    $infoUsuario = infoUser($user);
    include("../vista/viewUser/profile.php");
}

if($fotoSubmit){
    $nom = $_FILES["fotoUser"]["name"];
    $tipus = $_FILES["fotoUser"]["type"];
    $extension=substr(strstr($tipus,"/"),1);
    $grandaria = $_FILES["fotoUser"]["size"];

    if ($grandaria > 2000000) {
        exit();
    }else{
        session_start();
        $user = $_SESSION["user"];
        if (move_uploaded_file($_FILES["fotoUser"]['tmp_name'], "../vista/images/fotosUser/".$user.".".$extension)) {
           $url="".$user.".".$extension;
           subirFoto($url,$user);
        }
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }

}

if($submitBusinessSector){
    $businessSector = $_POST["businessSector"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateBusinessSector($businessSector,$user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitPostalCode){
    $postalCode = $_POST["postalCode"];
    session_start();
    $user = $_SESSION["user"];
    $success = updatePostalCode($postalCode,$user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitPassword){
    $password = $_POST["password"];
    session_start();
    $user = $_SESSION["user"];
    $success = updatePassword($password, $user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitIdiom){
    $idiom = $_POST["idiom"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateIdiom($idiom, $user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitCity){
    $city = $_POST["city"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateCity($city, $user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitCountry){
    $country = $_POST["country"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateCountry($country, $user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitEmail){
    $mail = $_POST["mail"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateMail($mail,$user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitPhone){
    $phone = $_POST["phone"];
    session_start();
    $user = $_SESSION["user"];
    $success = updatePhone($phone,$user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

if($submitDirection){
    $direction = $_POST["direction"];
    session_start();
    $user = $_SESSION["user"];
    $success = updateDirection($direction, $user);
    if($success){
        $usuario = sidebarUsuario($user);
        $infoUsuario = infoUser($user);
        include("../vista/viewUser/profile.php");
    }
}

?>