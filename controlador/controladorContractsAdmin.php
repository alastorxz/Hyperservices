
<?php

include ("../model/modelContractsAdmin.php");

$contractId = $_POST["id"];
$contractTypeSubmit = $_POST["contractTypeSubmit"];
$contractStatusSubmit = $_POST["contractStatusSubmit"];
$relatedProductSubmit = $_POST["relatedProductSubmit"];
$closeDate = $_POST["closeDate"];


if ($contractId){

    $infoContrato = infoContratoAdmin($contractId);
    session_start();
    $_SESSION["contractId"] = $contractId;
    $infoContractRelatedUser = infoContractRelatedUser($contractId);
    $infoContractRelatedProduct = infoContractRelatedProduct($contractId);
    $products = selectRelatedProduct();

    include ("../vista/viewAdmin/ContractAdminEdit.php");

}

if ($contractTypeSubmit){

    $contractType = $_POST["contractType"];
    if ($contractType){
        session_start();
        $update = updateContractType($_SESSION["contractId"],$contractType);
        if ($update){
            $products = selectRelatedProduct();
            $infoContrato = infoContratoAdmin($_SESSION["contractId"]);
            $infoContractRelatedUser = infoContractRelatedUser($_SESSION["contractId"]);
            $infoContractRelatedProduct = infoContractRelatedProduct($_SESSION["contractId"]);
            include ("../vista/viewAdmin/ContractAdminEdit.php");
        }
    }

}

if ($contractStatusSubmit){
     $contractStatus = $_POST["contractStatus"];
     if ($contractStatus){
     echo $contractStatus;
            session_start();
            $update = updateContractStatus($_SESSION["contractId"], $contractStatus);
            if ($update){
                $products = selectRelatedProduct();
                $infoContrato = infoContratoAdmin($_SESSION["contractId"]);
                $infoContractRelatedUser = infoContractRelatedUser($_SESSION["contractId"]);
                $infoContractRelatedProduct = infoContractRelatedProduct($_SESSION["contractId"]);
                include ("../vista/viewAdmin/ContractAdminEdit.php");

        }
    }

}

if ($closeDate){
    $hoy = date("Y-n-j");
    session_start();
    $update = updateCloseDate($_SESSION["contractId"], $hoy);
    if ($update){
        $products = selectRelatedProduct();
        $infoContrato = infoContratoAdmin($_SESSION["contractId"]);
        $infoContractRelatedUser = infoContractRelatedUser($_SESSION["contractId"]);
        $infoContractRelatedProduct = infoContractRelatedProduct($_SESSION["contractId"]);
        include ("../vista/viewAdmin/ContractAdminEdit.php");
    }
}

if ($relatedProductSubmit){
$relatedProduct = $_POST["relatedProduct"];

    if ($relatedProduct){
        session_start();
        $update = updateRelatedProduct($_SESSION["contractId"], $relatedProduct);
        if ($update){
            $infoContrato = infoContratoAdmin($_SESSION["contractId"]);
            $infoContractRelatedUser = infoContractRelatedUser($_SESSION["contractId"]);
            $infoContractRelatedProduct = infoContractRelatedProduct($_SESSION["contractId"]);
            $products = selectRelatedProduct();
            include ("../vista/viewAdmin/ContractAdminEdit.php");
        }
    }
}

?>

