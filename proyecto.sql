DROP DATABASE IF EXISTS proyecto;
CREATE DATABASE IF not exists proyecto;
USE proyecto;

CREATE TABLE user(
id INT auto_increment PRIMARY KEY,
userType ENUM('client','admin') NOT NULL,
firstName VARCHAR(30) NOT NULL,
lastName VARCHAR(50) NOT NULL,
nameUser VARCHAR(30)  NOT NULL,
passwd VARCHAR(30) NOT NULL,
direction VARCHAR(30)NOT NULL,
postalCode varchar(30),
mail VARCHAR(30) NOT NULL,
city VARCHAR(30) NOT NULL,
country VARCHAR(30) NOT NULL,
businessSector VARCHAR(50),
contactNumber VARCHAR(20) NOT NULL,
idiom VARCHAR(50),
image VARCHAR(200)
)ENGINE = InnoDB;

CREATE TABLE product(
id INT auto_increment PRIMARY KEY,
productName VARCHAR(50),
productType VARCHAR(30),
price double,
productDescription VARCHAR(2000),
image VARCHAR(250)
)ENGINE = InnoDB;

CREATE TABLE contract(
id INT auto_increment PRIMARY KEY,
contractType ENUM('No Support','Support','VIP') NOT NULL,
openedDate DATE,
closedDate DATE,
vigency INT,
contractStatus ENUM('Opened','Almost Expired','Expired'),
relatedProduct INT,
relatedUser INT,
FOREIGN KEY (relatedProduct) REFERENCES product(id),
FOREIGN KEY(relatedUser) REFERENCES user(id)
)ENGINE = InnoDB;

CREATE TABLE infoContract(
id INT auto_increment PRIMARY KEY,
contractType ENUM('No Support','Support','VIP') NOT NULL,
contractDescription VARCHAR(2000) NOT NULL,
price DOUBLE NOT NULL,
image VARCHAR(250)
)ENGINE = InnoDB;

CREATE TABLE cases(
id INT auto_increment PRIMARY KEY,
caseTitle VARCHAR(30),
caseDescription  VARCHAR(2000),
caseStatus ENUM('Opened', 'In Development','Closed'),
caseUrgency ENUM('Low','Medium','High'),
caseImpact ENUM('Low','Medium','High'),
caseResolution VARCHAR(2000),
relatedContract INT,
FOREIGN KEY (relatedContract) REFERENCES contract(id)
)ENGINE = InnoDB;

INSERT INTO user(userType, firstName, lastName, nameUser,passwd,direction, mail,city,country,contactNumber,image) VALUES('admin','admin','admin','admin','admin','admin','admin','admin','admin','admin','user.gif');

INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell PowerEdge R230','Servers for Racks',776.00,'Thanks to the 1-socket performance and capacity expandable internal storage, PowerEdge R230 is an excellent choice for use as the first server (rack) in small businesses and organizations.','dellPowerEdgeR230.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell PowerEdge R530','Servers for Racks',1088.00,'Chassis up to 8 hard drives 3.5" E5-2603 Intel Xeon processor, 8GB RAM, 1TB SATA 7200 rpm and 3 years of next business day service.','dellPowerEdgeR530.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell PowerEdge R630','Servers for Racks',1449.00,'Maximize data center efficiency engine with a database or ultra-dense virtualization that supports up to 24 SSDs flash in a 1U chassis.','dellPowerEdgeR630.jpg');

INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell Networking N1524P','Switches',1015.00,'Solution wing switch access Gigabit Ethernet (GbE) network of 24 ports and high energy efficiency with integrated 10GbE uplinks and PoE+.','dellSwitchN1524.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell Networking N1548P','Switches',1294.00,'Solution wing switch access Gigabit Ethernet (GbE) network of 48 ports and high energy efficiency with integrated 10GbE uplinks and PoE+.','dellSwitchN1548.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Dell Networking C9000','Switches',1377.00,'The Dell Networking C9000 is a modular switch highly scalable networks for data centers and markets mid-range business campus.','dellSwitchC9000.jpg');

INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Cisco RV042','Routers',130.97,'Securely connecting your small business to the outside world is as important as connecting your internal network devices to one another. Cisco Small Business RV Series Routers offer virtual private networking (VPN) technology that lets your remote workers connect to your network through a secure Internet pathway.','RV042.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Cisco RV016-G5','Routers',443.04,'It is an advanced network solution tethering to meet the needs of your small business. As other routers, allows multiple computers in your office share a connection to a private or public network. However, the 16 ports on this router feature unprecedented versatility.','RV016-G5.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('Cisco RV130W-E-K9-G5','Routers',121.93,'The Cisco RV130W Wireless-N Multifunction VPN Router is an easy-to-use, flexible, high-performance device well suited for small businesses. Now with web filtering, the new RV130W delivers highly secure, broadband, wired and wireless connectivity to small offices and remote employees. It can also be used either as a standalone wireless router, access point, bridge or repeater for flexible deployments, offering investment protection as your business needs evolve.','CiscoRV130W.jpg');

INSERT INTO product(productName, productType, price, productDescription, image) VALUES('D-Link Access Point DAP-1360','Access Point',35.00,'The DAP-1360 D-Link is a wireless access point in line with IEEE 802.11n. By connecting the DAP-1360 to a broadband router, you can share your high-speed Internet wirelessly, and create a secure wireless network to share photos, files, music, videos, printers and network storage','D-LinkWirelessAccessPointDAP1360.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('TP-LINK AC1900 Wireless Gigabit Access Point','Access Point',149.00,'With the upcoming 802.11ac Wi-Fi technology, D9 Archer delivers data transfer rates superfast to 1900Mbps. Simultaneous dual-band offers the flexibility of two dedicated networks to avoid interference for better online experience.','TP-LINKAC1900.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('LINKSYS LAPAC1200 BUSINESS','Access Point',274.99,'The key element to provide your business with a powerful and secure WiFi connectivity Provides the next generation of WiFi technology (802.11ac) to offer transfer speeds of up AC1200 Mbps. Provides next-generation 802.11ac wireless technology to transfer speeds up to 3 times faster than 802.11n.','LynksysLAPAC1200.jpg');

INSERT INTO product(productName, productType, price, productDescription, image) VALUES('HP LaserJet Enterprise Flow M830z','Printers',10888.80,'Get more effective in your work thanks to the NFC mobile printing technology (Near Field Communication) which lets you print from your mobile phone wirelessly.','hpLaserJetEnterpriseFlowM830z.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('HP LaserJet Enterprise Flow M630z','Printers',4948.19,'This HP printer Enterprise accelerates productivity and optimizes workflows of the company, protects data and documents thanks to professional security functions, while providing a seamless mobile printing. Hard disk encryption, input capacity of 500 sheets, stapling mailbox for 900 sheets, fax and fax cable.','HPLaserJetEnterpriseFLowM630z.jpg');
INSERT INTO product(productName, productType, price, productDescription, image) VALUES('HP Officejet Enterprise Color Flow X585z ','Printers',4768.82,'This multifunctional HP Officejet offers color printing with up to twice the speed and half the cost per page than color laser also copy, scan and fax. It is designed for advanced workflow and built to last. Resistant keyboard, HP Precision feeder Every Page, scanner and OCR great resistance.','HPOfficeJetEnterpriseColorFlowX585z.jpg');

INSERT INTO infoContract(contractType,contractDescription,price,image) VALUES ('No Support','This contract does not provide maintenance support or other benefits to the user..',0.00,'NoCustomerSupport.png');
INSERT INTO infoContract(contractType,contractDescription,price,image) VALUES ('Support','This contract provides maintenance support 12/5, but does not provide additional benefits to the user.',60.00,'CustomerSupport.png');
INSERT INTO infoContract(contractType,contractDescription,price,image) VALUES ('VIP','This contract provides maintenance support 24/7, offers other that their issues can be resolved early and as quickly as possible in a maximum term of 24 hours.',80.00,'PremiumSupport.png');