$( document ).ready(function() {
    $("#cityForm").hide();
    $("#countryForm").hide();
    $("#businessForm").hide();
    $("#showLessForm").hide();
    // $("#mailForm").hide();
    // $("#showLessEmail").hide();
    // $("#postalCodeForm").hide();
    // $("#showLessPostalForm").hide();
    // $("#idiomForm").hide();
    // $("#showLessIdiom").hide();
    // $("#phoneForm").hide();
    // $("#showLessPhone").hide();
    // $("#showLessDirection").hide();
    // $("#directionForm").hide();

    $("#viewPassword").click(function(){
        if($("#glyphicon").hasClass("glyphicon glyphicon-eye-open")){
            $("#glyphicon").removeClass("glyphicon glyphicon-eye-open");
            $("#glyphicon").addClass("glyphicon glyphicon-eye-close");
            document.getElementById('password').type = 'text';
        }else if($("#glyphicon").hasClass("glyphicon glyphicon-eye-close")){
            $("#glyphicon").removeClass("glyphicon glyphicon-eye-close");
            $("#glyphicon").addClass("glyphicon glyphicon-eye-open");
            document.getElementById('password').type = 'password';
        }
    });
    $("#editbusinessSector").click(function(){
        $("#city").hide();
        $("#cityForm").show();
        $("#country").hide();
        $("#countryForm").show();
        $("#businessSector").hide();
        $(this).hide();
        $("#businessForm").show();
        $("#showLessForm").show();
    });

    $("#showLessForm").click(function(){
        $("#cityForm").hide();
        $("#countryForm").hide();
        $("#businessForm").hide();
        $(this).hide();
        $("#city").show();
        $("#country").show();
        $("#businessSector").show();
        $("#editbusinessSector").show();
    });

    // $("#editEmail").click(function() {
    //     $("#mail").hide();
    //     $(this).hide();
    //     $("#mailForm").show();
    //     $("#showLessEmail").show();
    // });
    //
    // $("#showLessEmail").click(function(){
    //     $("#mail").show();
    //     $(this).hide();
    //     $("#mailForm").hide();
    //     $("#editEmail").show();
    // });

    // $("#showFormPostal").click(function(){
    //     $(this).hide();
    //     $("#postalCode").hide();
    //     $("#postalCodeForm").show();
    //     $("#showLessPostalForm").show();
    // });
    //
    // $("#showLessPostalForm").click(function(){
    //     $(this).hide();
    //     $("#postalCodeForm").hide();
    //     $("#postalCode").show();
    //     $("#showFormPostal").show();
    // });
    //
    // $("#showFormIdiom").click(function(){
    //     $(this).hide();
    //     $("#idiom").hide();
    //     $("#idiomForm").show();
    //     $("#showLessIdiom").show();
    // });
    //
    // $("#showLessIdiom").click(function(){
    //     $(this).hide();
    //     $("#idiomForm").hide();
    //     $("#idiom").show();
    //     $("#showFormIdiom").show();
    // });
    //
    // $("#showPhoneForm").click(function(){
    //     $("#phone").hide();
    //     $(this).hide();
    //     $("#phoneForm").show();
    //     $("#showLessPhone").show();
    // });
    //
    // $("#showLessPhone").click(function(){
    //     $(this).hide();
    //     $("#phoneForm").hide();
    //     $("#phone").show();
    //     $("#showPhoneForm").show();
    // });
    //
    // $("#showDirectionForm").click(function(){
    //     $(this).hide();
    //     $("#direction").hide();
    //     $("#directionForm").show();
    //     $("#showLessDirection").show();
    // });
    //
    // $("#showLessDirection").click(function(){
    //     $(this).hide();
    //     $("#directionForm").hide();
    //     $("#direction").show();
    //     $("#showDirectionForm").show();
    // });

});

