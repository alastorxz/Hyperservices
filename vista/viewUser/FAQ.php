<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/dashProducts.css" rel="stylesheet">

<!--     Just for debugging purposes. Don't actually copy these 2 lines!
    [if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
          <form method="post" action="../controlador/controladorPortal.php">
            <div id="navbar" class="navbar-collapse collapse">
              <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
            </div>
          </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="nav nav-sidebar">
                          <?php
                          for ($i=0;$i<count($usuario);$i++){
                          ?>

                          <img src="../vista/images/fotosUser/<?php echo $usuario[$i]["image"]?>" class="img-responsive profileImage" alt="Generic placeholder thumbnail">
                          <p class="profileText"> Username: <?php echo $usuario[$i]["nameUser"] ?> </p>
                          <p class="profileText"> <?php echo ($usuario[$i]["firstName"]." ".$usuario[$i]["lastName"]); ?> </p>
                          <p class="profileText"> <?php echo $usuario[$i]["mail"] ?> </p>

                          <?php
                          }
                          ?>

                      </div>
                   <div class="line-separator"></div>
          <ul class="nav nav-sidebar">
           <li>
               <form method="post" action="../controlador/controladorPortal.php">
                <button type="submit" name="dashboard" value="dashboard" class="btn btn-default contracts-link"><span>Dashboard</span></button>
               </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="cases" value="cases" class="btn btn-default contracts-link"><span>Cases</span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="contracts" value="contracts" class="btn btn-default contracts-link"><span>Contracts</span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="profile" value="profile" class="btn btn-default contracts-link"><span>Profile</span></button>
                </form>
            </li>
            <li>
              <form method="post" action="../controlador/controladorPortal.php">
                <button type="submit" name="FAQ" value="FAQ" class="btn btn-primary dashboard-link"><span>FAQ</span></button>
              </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="row placeholders">
          <h3> How does our page work?</h3>

          <p> Before you can use our service, you must create a contract. For that, we advice you to go to our dashboard and see all the contracts and products we provide you. </p>
          <hr>

          <h3> How I create a new Contract?  </h3>
          <p> Creating a contract is an easy task. First of all, go to our left sidebar and select "CONTRACTS". On this new page, you can see all your created contracts
           and create a new One. To create a new Contract, you must click the + button on the lower-right side of the page. After that it will apear a new formulary
           that will ask you the contract type and the related product of the contract. BE SURE TO KNOW WHICH PRODUCT YOU WILL EARN FROM US!
           </p>
           <hr>
           <h3> I have a contract, and now? </h3>
          <p> Congratulations!! You have already a contract with us! Now you can create cases! For that, you must access to our sidebar and select the Cases button.date
           Inside the cases page, you can see your actual incidents. If you have any problem with any of our rent products, you must create a new Case so we can fix
           the problem as soon as posible. If you click on the + button on the lower-right side of the page, you will be redirect to a new page to create a new case.
           </p>
           <hr>

           <h3> I want to change my profile avatar </h3>
            <p> To change your profile avatar, you must go to the profile button on the left sidebar. At the upper part of the page, you will see your avatar and some sort of buttons
            to change your avatar.
            </p>
            <hr>

           <h3> I have a problem with my profile data and I want to change it, how can I change my information? </h3>
            <p> To change your profile information, you only have to click on the profile button at the left sidebar of the page. Here you will see your information and if there is
            something that is wrong, you can change it instantly by inserting the new information on each input and by clicking on change button.
            </p>
            <hr>

             <h3> My contract is going to expire or is already expired! How can i renew it? </h3>
             <p> For the moment, there is no way to change that automatically, so you must contact with us at this e-mail: proyectohyperservices@gmail.com, indicating your profile information
             and contract id. From there, we will send to your e-mail the instructions to renew your countract
             </p>
             <hr>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body></html>