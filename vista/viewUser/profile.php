<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <script src="../vista/js/jquery-2.1.3.js"></script>
    <script src="../vista/js/perfil.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/user/profile.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
         <form method="post" action="../controlador/controladorPortal.php">
           <div id="navbar" class="navbar-collapse collapse">
            <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
           </div>
         </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="nav nav-sidebar">
            <?php
            for ($i=0;$i<count($usuario);$i++){
            ?>

            <img src="../vista/images/fotosUser/<?php echo $usuario[$i]["image"]?>" class="img-responsive profileImage" alt="Generic placeholder thumbnail">
            <p class="profileText"> Username: <?php echo $usuario[$i]["nameUser"] ?> </p>
            <p class="profileText"> <?php echo ($usuario[$i]["firstName"]." ".$usuario[$i]["lastName"]); ?> </p>
            <p class="profileText"> <?php echo $usuario[$i]["mail"] ?> </p>

            <?php
            }
            ?>
        </div>
        <div class="line-separator"></div>
            <ul class="nav nav-sidebar">
                <li>
                    <form method="post" action="../controlador/controladorPortal.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="dashboard" value="dashboard"><span>Dashboard<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorPortal.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="cases" value="cases"><span>Cases<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorPortal.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="contracts" value="contracts"><span>Contracts<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorPortal.php">
                        <button class="btn btn-primary contract-link" type="submit" name="profile" value="profile"><span>Profile<span></button>
                    </form>
                </li>
                <li>
                  <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="FAQ" value="FAQ" class="btn btn-default dashBoard-link"><span>FAQ</span></button>
                  </form>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php for($i=0; $i<count($infoUsuario);$i++){ ?>
            <header class="jumbotron hero-spacer fotoUsuario">
                <div class="row">
                    <div class="col-sm-5">
                        <img src="../vista/images/fotosUser/<?php echo $infoUsuario[$i]["image"];?>" class="img-responsive" alt="User Foto">
                        <form method="post" action="../controlador/controladorPerfil.php" enctype="multipart/form-data">
                            <fieldset class="form-group">
                                <label for="fotoUser">File input</label>
                                <input type="file" name="fotoUser">
                                <button class="btn btn-default" type="submit" name="loadFoto" value="loadFoto"><span>Upload Foto</span></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="col-sm-7">
                        <h2><?php echo $infoUsuario[$i]["firstName"]; echo " ".$infoUsuario[$i]["lastName"];?></h2>
                        <p><?php echo $infoUsuario[$i]["nameUser"]; ?></p>
                        <p>
                            <span id="city"><?php echo $infoUsuario[$i]["city"]; ?></span>
                            <span id="country"><?php echo ", ".$infoUsuario[$i]["country"]; ?> | </span>
                            <?php if($infoUsuario[$i]["businessSector"]){ ?>
                            <span id="businessSector"><?php echo $infoUsuario[$i]["businessSector"]; ?></span>
                            <button id="editbusinessSector" type="submit" name="editBusinessSector" value="editBusinessSector" class="editBusinessSector">
                                <span class="glyphicon glyphicon-edit"></span>
                            </button>
                            <?php }else{ ?>
                            <form method="post" action="../controlador/controladorPerfil.php" class="businessInput">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="businessSector" value="Business Sector" required/>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 divSubmitBusinessSector">
                                        <button class="btn btn-default" type="submit" name="submitBusinessSector" value="submitBusinessSector">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                            <form id="cityForm" method="post" action="../controlador/controladorPerfil.php" class="cityInput">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="city" value="<?php echo $infoUsuario[$i]["city"];?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" name="submitCity" value="submitCity" class="btn btn-default">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <form id="countryForm" method="post" action="../controlador/controladorPerfil.php" class="countryInput">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="country" value="<?php echo $infoUsuario[$i]["country"];?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" name="submitCountry" value="submitCountry" class="btn btn-default">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <form id="businessForm" method="post" action="../controlador/controladorPerfil.php" class="businessInput2">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="businessSector" value="<?php echo $infoUsuario[$i]["businessSector"];?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" type="submit" name="submitBusinessSector" value="submitBusinessSector">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <button class="btn btn-default showLessForm" id="showLessForm" type="submit" name="showLessForm" value="showLessForm">
                                <span class="glyphicon glyphicon-arrow-up"></span>
                            </button>
                        </p>
                </div>
            </header>
            <hr>
            <div class="jumbotron hero-spacer">
                <div class="row">
                    <div class="col-sm-6">
                        <p><span>Username: <?php echo $infoUsuario[$i]["nameUser"]; ?></span></p>
                        <p><span>Mail: <span id="mail"><?php echo $infoUsuario[$i]["mail"]; ?></span></span>

                        </p>
                            <form id="mailForm" method="post" action="../controlador/controladorPerfil.php" >
                                <div class="row">
                                    <div class="col-sm-7 divInputEmail">
                                        <div class="form-group">
                                        <input class="form-control" type="email" name="mail" value="<?php echo $infoUsuario[$i]["mail"]; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 divEditEmail">
                                        <button id="editEmail" type="submit" class="btn btn-default" name="submitEmail" value="submitEmail">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>

                        <p>
                            <span>Postal Code:
                            <?php if($infoUsuario[$i]["postalCode"]){?>
                                <span id="postalCode"><?php echo $infoUsuario[$i]["postalCode"];?></span>
                                <form id="postalCodeForm"  method="post" action="../controlador/controladorPerfil.php">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <input class="form-control" type="text" name="postalCode" value="<?php echo $infoUsuario[$i]["postalCode"]; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4 divEditPostal">
                                                                        <button class="btn btn-default" type="submit" name="submitPostalCode" value="submitPostalCode">
                                                                            Change
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>

                            <?php }else{ ?>
                                <form id="postalForm" method="post" action="../controlador/controladorPerfil.php" >
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="postalCode" value="00000" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-default" type="submit" name="submitPostalCode" value="submitPostalCode">
                                                Change
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            <?php } ?>
                            </span>

                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3">
                                <p><span>Password:</span><p>
                            </div>
                            <div class="col-sm-7">
                                <form method="post" action="../controlador/controladorPerfil.php" class="passwordInput">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" id="password" type="password" name="password" value="<?php echo $infoUsuario[$i]["password"];?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 divChangePassword">
                                            <button class="btn btn-default" type="submit" name="submitPassword" value="submitPassword">
                                                Change
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-default viewPassword" id="viewPassword" type="submit" name="viewPassword" value="viewPassword">
                                    <span id="glyphicon" class="glyphicon glyphicon-eye-open"></span>
                                </button>
                            </div>
                        </div>
                        <p>
                            <span>Phone:
                                <span id="phone"><?php echo $infoUsuario[$i]["contactNumber"];?></span>
                            </span>

                            <form id="phoneForm" method="post" action="../controlador/controladorPerfil.php">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="phone" value="<?php echo $infoUsuario[$i]["contactNumber"];?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 divSubmitPhone">
                                        <button class="btn btn-default" type="submit" name="submitPhone" value="submitPhone">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </p>
                        <p>
                            <span>Direction:
                                <span id="direction"><?php echo $infoUsuario[$i]["direction"];?></span>
                            </span>

                            <form id="directionForm"  method="post" action="../controlador/controladorPerfil.php">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="direction" value="<?php echo $infoUsuario[$i]["direction"];?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 divSubmitDirection">
                                        <button class="btn btn-default" type="submit" name="submitDirection" value="submitDirection">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </p>
                    </div>
                </div>
                <hr>
                <div>
                    <p class="pIdiom">
                        <span>Idiom:
                        <?php if($infoUsuario[$i]["idiom"]){ ?>
                            <span id="idiom"><?php echo $infoUsuario[$i]["idiom"]; ?></span>
                            <form id="idiomForm" method="post" action="../controlador/controladorPerfil.php">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="text" name="idiom" value="<?php echo $infoUsuario[$i]["idiom"]; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 divSubmitIdiom">
                                                                <button class="btn btn-default" type="submit" name="submitIdiom" value="submitIdiom">
                                                                    Change
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                        <?php }else{ ?>
                            <form method="post" action="../controlador/controladorPerfil.php">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="idiom" value="Idiom" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-default" type="submit" name="submitIdiom" value="submitIdiom">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                        </span>


                    </p>
                </div>
            </div>
            <?php } ?>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>



</body></html>