<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/user/contracts.css" rel="stylesheet">


  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
          <form method="post" action="../controlador/controladorPortal.php">
                      <div id="navbar" class="navbar-collapse collapse">
                         <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
                      </div>
          </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
       <div class="nav nav-sidebar">
                         <?php
                         for ($i=0;$i<count($usuario);$i++){
                         ?>

                         <img src="../vista/images/fotosUser/<?php echo $usuario[$i]["image"]?>" class="img-responsive profileImage" alt="Generic placeholder thumbnail">
                         <p class="profileText"> Username: <?php echo $usuario[$i]["nameUser"] ?> </p>
                         <p class="profileText"> <?php echo ($usuario[$i]["firstName"]." ".$usuario[$i]["lastName"]); ?> </p>
                         <p class="profileText"> <?php echo $usuario[$i]["mail"] ?> </p>

                         <?php
                         }
                         ?>

                     </div>
                  <div class="line-separator"></div>

          <ul class="nav nav-sidebar">
            <li class="#">
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="dashboard" value="dashboard"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="cases" value="cases"><span>Cases<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-primary contract-link" type="submit" name="contracts" value="contracts"><span>Contracts<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="profile" value="profile"><span>Profile<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                   <button type="submit" name="FAQ" value="FAQ" class="btn btn-default dashBoard-link"><span>FAQ</span></button>
                 </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Contracts</h1>
          <div class="row placeholders">
           <?php
                           for($i=0;$i< count($infoContratosUser);$i++){
                       ?>
                       <?php
                           if($infoContratosUser[$i]["contractStatus"] == "Opened"){ ?>
                               <div class="list-group">
                                   <p class="list-group-item-text"><span>Contract ID: </span><?php echo $infoContratosUser[$i]["id"]; ?></p>
                                   <p class="list-group-item-text"><span>Contract Type: </span><?php echo $infoContratosUser[$i]["contractType"]; ?></p>
                                   <p class="list-group-item-text"><span>Opened Date: </span><?php echo $infoContratosUser[$i]["openedDate"]; ?></p>
                                   <p class="list-group-item-text"><span>Closed Date: </span><?php echo $infoContratosUser[$i]["closedDate"]; ?></p>
                                   <p class="list-group-item-text"><span>Contract Status: </span><?php echo $infoContratosUser[$i]["contractStatus"]; ?></p>
                                   <p class="list-group-item-text"><span>Related Product: </span><?php echo $infoContratosUser[$i]["relatedProduct"]; ?></p>

                                 <div class="imagen">
                                   <img src="../vista/images/contractStatus/tick.png">
                                 </div>
                               </div>
                               <hr>
                               <?php }else if($infoContratosUser[$i]["contractStatus"] == "Almost Expired"){ ?>
                                <div class="list-group">
                                  <p class="list-group-item-text"><span>Contract ID: </span><?php echo $infoContratosUser[$i]["id"]; ?></p>
                                  <p class="list-group-item-text"><span>Contract Type: </span><?php echo $infoContratosUser[$i]["contractType"]; ?></p>
                                  <p class="list-group-item-text"><span>Opened Date: </span><?php echo $infoContratosUser[$i]["openedDate"]; ?></p>
                                  <p class="list-group-item-text"><span>Closed Date: </span><?php echo $infoContratosUser[$i]["closedDate"]; ?></p>
                                  <p class="list-group-item-text"><span>Contract Status: </span><?php echo $infoContratosUser[$i]["contractStatus"]; ?></p>
                                  <p class="list-group-item-text"><span>Related Product: </span><?php echo $infoContratosUser[$i]["relatedProduct"]; ?></p>

                                 <div class="imagen">
                                   <img src="../vista/images/contractStatus/warning.png">
                                 </div>
                               </div>
                               <hr>
                               <?php }else if($infoContratosUser[$i]["contractStatus"] == "Expired"){ ?>
                                   <div class="list-group">

                                    <p class="list-group-item-text"><span>Contract ID: </span><?php echo $infoContratosUser[$i]["id"]; ?></p>
                                    <p class="list-group-item-text"><span>Contract Type: </span><?php echo $infoContratosUser[$i]["contractType"]; ?></p>
                                    <p class="list-group-item-text"><span>Opened Date: </span><?php echo $infoContratosUser[$i]["openedDate"]; ?></p>
                                    <p class="list-group-item-text"><span>Closed Date: </span><?php echo $infoContratosUser[$i]["closedDate"]; ?></p>
                                    <p class="list-group-item-text"><span>Contract Status: </span><?php echo $infoContratosUser[$i]["contractStatus"]; ?></p>
                                    <p class="list-group-item-text"><span>Related Product: </span><?php echo $infoContratosUser[$i]["relatedProduct"]; ?></p>

                                     <div class="imagen">
                                       <img src="../vista/images/contractStatus/expired.png">
                                     </div>
                                   </div>
                                   <hr>
                       <?php
                           }
                       }
                       ?>


          </div>
          <div class="newContractButton">
              <form method="post" action="../controlador/controladorPortal.php">
                  <button type="submit" name="newContract" value="newContract" class="btn btn-primary">
                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                  </button>
              </form>
          </div>
          <?php if($success == true){ ?>
              <div class="messageSuccess" style="border-color: #2e6da4;">
                <h2>Success Message</h2>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-primary btnCloseSuccessMessage" type="submit" value="closeContractForm" name="closeContractForm"><span class="glyphicon glyphicon-remove"></span></button>
                </form>
                <p>Your Contract was created successfully</p>
              </div>
          <?php } ?>
          <?php if($showForm == true){ ?>
                <div id="newContractInfo" class="newContractInfo" style="border-color: #2e6da4;">
                    <h2>Contracts: Create Contract</h2>
                    <form method="post" action="../controlador/controladorPortal.php">
                        <button class="btn btn-primary btnCloseContractForm" type="submit" value="closeContractForm" name="closeContractForm"><span class="glyphicon glyphicon-remove"></span></button>
                    </form>
                    <form method="post" action="../controlador/controladorPortal.php" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="contractType">Select contract type:</label>
                            <select required name="contractSelected" class="form-control" id="contractType">
                                <option value=""></option>
                                <?php for ($i=0; $i < count($contratos); $i++){ ?>
                                    <option value="<?php echo $contratos[$i]["Type"]; ?>"><?php echo $contratos[$i]["Type"]; ?></option>
                                <?php } ?>
                            </select>
                            <br>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="relatedProduct">Select related product:</label>
                            <select required name="productSelected" class="form-control" id="relatedProduct">
                                <option value=""></option>
                                <?php
                                    $product_type='';
                                    for ($i=0; $i < count($productos); $i++){
                                        if($product_type != $productos[$i]["productType"]){
                                            if($product_type !=''){
                                                echo '</optgroup>';
                                            }
                                            echo '<optgroup label="'.$productos[$i]["productType"].'">';
                                        }
                                        echo '<option value="'.$productos[$i]["productName"].'">'.$productos[$i]["productName"].'</option>';
                                        $product_type = $productos[$i]["productType"];
                                    }
                                    if($product_type !=''){
                                        echo '</optgroup>';
                                    }
                                ?>
                            </select>
                            <br>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-default" value="createNewContract" name="createNewContract">Create New Contract</button>
                            </div>
                        </div>
                    </form>
                </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body></html>