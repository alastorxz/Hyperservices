<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/user/cases.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
         <form method="post" action="../controlador/controladorPortal.php">
             <div id="navbar" class="navbar-collapse collapse">
                <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
             </div>
         </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="nav nav-sidebar">
          <?php
          for ($i=0;$i<count($usuario);$i++){
          ?>

          <img src="../vista/images/fotosUser/<?php echo $usuario[$i]["image"]?>" class="img-responsive profileImage" alt="Generic placeholder thumbnail">
          <p class="profileText"> Username: <?php echo $usuario[$i]["nameUser"] ?> </p>
          <p class="profileText"> <?php echo ($usuario[$i]["firstName"]." ".$usuario[$i]["lastName"]); ?> </p>
          <p class="profileText"> <?php echo $usuario[$i]["mail"] ?> </p>

          <?php
          }
          ?>

        </div>
        <div class="line-separator"></div>
          <ul class="nav nav-sidebar">
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="dashboard" value="dashboard"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-primary cases-link" type="submit" name="cases" value="cases"><span>Cases<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="contracts" value="contracts"><span>Contracts<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="profile" value="profile"><span>Profile<span></button>
                </form>
            </li>
            <li>
                 <form method="post" action="../controlador/controladorPortal.php">
                   <button type="submit" name="FAQ" value="FAQ" class="btn btn-default dashBoard-link"><span>FAQ</span></button>
                 </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Cases</h1>
          <div class="row placeholders">
            <div class="list-group">
                <form method="post" action="../controlador/controladorIncidencias.php">
             <?php
                for($i=0;$i<count($mostrar);$i++){
             ?>
                <div class="list-group casesPosition">
                 <button type="submit" name="id" value=<?php echo $mostrar[$i]["id"]?> class="list-group-item">
                     <h4 class="list-group-item-heading"><?php echo $mostrar[$i]["caseTitle"]; ?></h4>
                     <p class="list-group-item-text marginElements divTextArea"><span>Case Description: </span><?php echo $mostrar[$i]["caseDescription"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Status: </span><?php echo $mostrar[$i]["caseStatus"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Urgency: </span><?php echo $mostrar[$i]["caseUrgency"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Impact: </span><?php echo $mostrar[$i]["caseImpact"]; ?></p>

                     <?php if ($mostrar[$i]["caseResolution"] != ""){ ?>
                            <p class="list-group-item-text marginElements"><span>Case Resolution: </span><?php echo $mostrar[$i]["caseResolution"]; ?></p>
                     <?php
                           }
                     ?>

                     <p class="list-group-item-text marginElements"><span>Related Contract: </span><?php echo $mostrar[$i]["relatedContract"]; ?></p>
                 </button>
               </div>
             <?php
               }
             ?>

            </div>
            </form>
             <div class="newCaseButton">
                <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="newCase" value="newCase" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </form>
             </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body></html>