<?php
session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/dashContracts.css" rel="stylesheet">

    <script src="../vista/js/jquery-2.1.3.js"></script>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
            <form method="post" action="../controlador/controladorPortal.php">
                      <div id="navbar" class="navbar-collapse collapse">
                         <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
                      </div>
            </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">

        <div class="nav nav-sidebar">
                          <?php
                          for ($i=0;$i<count($usuario);$i++){
                          ?>

                          <img src="../vista/images/fotosUser/<?php echo $usuario[$i]["image"]?>" class="img-responsive profileImage" alt="Generic placeholder thumbnail">
                          <p class="profileText"> Username: <?php echo $usuario[$i]["nameUser"] ?> </p>
                          <p class="profileText"> <?php echo ($usuario[$i]["firstName"]." ".$usuario[$i]["lastName"]); ?> </p>
                          <p class="profileText"> <?php echo $usuario[$i]["mail"] ?> </p>

                          <?php
                          }
                          ?>

                      </div>
                   <div class="line-separator"></div>
          <ul class="nav nav-sidebar">
            <li class="active">
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-primary dashBoard-link" type="submit" name="dashboard" value="dashboard"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default contract-link" type="submit" name="cases" value="cases"><span>Cases<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default contract-link" type="submit" name="contracts" value="contracts"><span>Contracts<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button class="btn btn-default contract-link" type="submit" name="profile" value="profile"><span>Profile<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                  <button type="submit" name="FAQ" value="FAQ" class="btn btn-default contract-link"><span>FAQ</span></button>
                </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>
          <ul class="nav nav-pills">
            <li>
                <form method="post" action="../controlador/controladorPortal.php">
                    <button type="submit" name="products" value="products" class="btn btn-default boton-link">Products</button>
                </form>
            </li>
            <li class="active"><a href="#">Contracts</a></li>
          </ul>

          <div class="row placeholders">
            <?php
                for($i=0; $i<count($contratos); $i++){
            ?>
                    <div class="col-xs-6 col-sm-4 placeholder">
                        <img src="../vista/images/contracts/<?php echo $contratos[$i]["image"]?>" class="img-responsive" alt="Generic placeholder thumbnail" height="200" width="200">
                        <h4><?php echo $contratos[$i]["Type"]; ?></h4><br>
                        <p>
                            <?php if($contratos[$i]["price"] == "0"){ ?>
                                <strong>It's FREE!!</strong>
                            <?php }else{ ?>
                                <strong><?php echo $contratos[$i]["price"]; ?>€</strong>
                            <?php } ?>
                        </p>
                        <div style="text-align: justify">
                            <span class="text-muted"><?php  echo $contratos[$i]["description"];?></span>
                        </div>
                    </div>
                <?php
                }
                ?>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>


</body></html>