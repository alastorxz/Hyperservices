<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/admin/contractsEdit.css" rel="stylesheet">


<!--     Just for debugging purposes. Don't actually copy these 2 lines!
    [if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

   <body>

      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
              <?php
                  echo "Welcome ". $_SESSION["user"];
              ?>
            </a>
          </div>
         <form method="post" action="../controlador/controladorAdmin.php">
                 <div id="navbar" class="navbar-collapse collapse">
                    <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
                 </div>
         </form>
        </div>
      </nav>

      <div class="container-fluid">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
               <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                </form>
               </li>
               <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-primary contract-link " type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                </form>
               </li>
               <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                </form>
               </li>
               <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                </form>
               </li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php
                                for($i = 0; $i<count($infoContrato); $i++){
                            ?>
            <div class="jumbotron hero-spacer">
                <h2 class="list-group-item-text" > CONTRACT INFORMATION </h2>


                <p class="list-group-item-text"><span>Contract ID: </span><?php echo $infoContrato[$i]["id"]; ?></p>

                    <p><span> Opened Date: <?php echo $infoContrato[$i]["openedDate"]; ?></p>

                    <p><span> Related User: <?php echo $infoContractRelatedUser; ?></p>
             </div>

          <div class="jumbotron hero-spacer">


              <h2> CONTRACT EDITION </h2>
              <br>

               <p>Contract Type: <?php echo $infoContrato[$i]["contractType"] ?> </p>
               <div class="row">
                 <form method ="POST" action="../controlador/controladorContractsAdmin.php">
                           <div class="form-group">
                           <div class="col-sm-10">
                                 <select  class="form-control" name="contractType">
                                    <?php if ($infoContrato[$i]["contractType"] == "VIP"){  ?>
                                        <option value="Vip"> <?php echo $infoContrato[$i]["contractType"] ?></option>
                                        <option value="No support">No support </option>
                                        <option value="Support"> Support </option>
                                    <?php } else if ($infoContrato[$i]["contractType"] == "No Support") { ?>
                                         <option value="No Support"> <?php echo $infoContrato[$i]["contractType"] ?></option>
                                         <option value="VIP">VIP </option>
                                          <option value="Support"> Support </option>
                                    <?php } else if ($infoContrato[$i]["contractType"] == "Support") { ?>
                                          <option value="Support"> <?php echo $infoContrato[$i]["contractType"] ?></option>
                                          <option value="No support">No support </option>
                                          <option value="VIP"> VIP </option>
                                    <?php }?>
                                 </select>
                            </div>
                    </div>
                   <div class="col-sm-2">
                        <button  class="btn btn-default" type="submit" name="contractTypeSubmit" value="contractTypeSubmit">
                            <span> Change </span>
                        </button>
                   </div>
               </form>
               </div>
               <hr>

               <p> Contract Status:  <?php echo $infoContrato[$i]["contractStatus"] ?></p>
               <div class="row">
               <form action="../controlador/controladorContractsAdmin.php" method="POST">
                    <div class="col-sm-10">
                        <div class="form-group">
                            <select class="form-control" name="contractStatus">
                              <?php if ($infoContrato[$i]["contractStatus"] == "Opened"){ ?>
                                          <option value="Opened"> <?php echo $infoContrato[$i]["contractStatus"] ?></option>
                                          <option value="Almost Expired"> Almost Expired</option>
                                          <option value="Expired"> Expired </option>

                              <?php } else if ($infoContrato[$i]["contractStatus"] == "Almost Expired"){ ?>
                                          <option value="Almost Expired"> <?php echo $infoContrato[$i]["contractStatus"] ?></option>
                                          <option value="Opened"> Opened</option>
                                          <option value="Expired"> Expired </option>

                              <?php } else if ($infoContrato[$i]["contractStatus"] == "Expired"){ ?>
                                          <option value="Expired"> <?php echo $infoContrato[$i]["contractStatus"] ?></option>
                                          <option value="Almost Expired"> Almost Expired</option>
                                          <option value="Opened"> Opened </option>

                              <?php }?>
                              </select>
                          </div>
                    </div>

                    <div class="col-sm-2">
                      <button class="btn btn-default" type="submit" name="contractStatusSubmit" value="contractStatusSubmit">
                         <span> Change </span>
                      </button>
                    </div>
               </form>
               </div>
               <hr>

               <p>Related Product: <?php echo $infoContractRelatedProduct; ?> </p>
               <div class="row">
                <form method="POST" action="../controlador/controladorContractsAdmin.php">

                    <div class="col-sm-10">
                        <div class="form-group">
                                <select class="form-control" name="relatedProduct">
                                      <option value=""></option>
                                       <?php
                                        $product_type='';
                                        for ($j=0; $j < count($products); $j++){
                                        if($product_type != $products[$j]["productType"]){
                                        if($product_type !=''){
                                         echo '</optgroup>';
                                         }
                                         echo '<optgroup label="'.$products[$j]["productType"].'">';
                                         }
                                         echo '<option value="'.$products[$j]["id"].'">'.$products[$j]["productName"].'</option>';
                                         $product_type = $products[$j]["productType"];
                                         }
                                         if($product_type !=''){
                                         echo '</optgroup>';
                                         }
                                         ?>
                                  </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-default " type="submit" name="relatedProductSubmit" value="relatedProductSubmit">
                            <span> Change </span>
                         </button>
                    </div>

                </form>
                </div>
                <hr>
                <div class="row">
                <form method ="POST" action="../controlador/controladorContractsAdmin.php">
                    <div class="col-sm-10">
                              <div class="form-group">
                              <h4> Closed Date: <?php echo $infoContrato[$i]["closedDate"]; ?>
                              <button class ="btn btn-default" type="submit" name="closeDate" value="closeDate">
                                <span> Close Contract </span>
                              </button></h4>
                              </div>
                    </div>
                </form>
                </div>

            <?php

              }
             ?>

        </div>
      </div>

      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="../vista/js/jquery-1.11.3.min.js"></script>
      <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
      <script src="../../dist/js/bootstrap.min.js"></script>
      <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
      <script src="../../assets/js/vendor/holder.min.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
  </html>