<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <script src="../vista/js/jquery-2.1.3.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/admin/contractsInfoDetail.css" rel="stylesheet">

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>

        <form method="post" action="../controlador/controladorAdmin.php">
        <div id="navbar" class="navbar-collapse collapse">
           <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
        </div>
        </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-primary contract-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashboard-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <?php for($i=0;$i<count($contractInfo); $i++) {?>


          <header class="jumbotron hero-spacer">
            <div class="infoHeaderContract">
              <h2>Contract type:  <?php echo $contractInfo[$i]["contractType"]; ?> </h2>
             </div>
            <div class="">
                <img src="../vista/images/contracts/<?php echo $contractInfo[$i]["image"];?>" class="img-responsive" alt="Contract Foto">
            </div>

          </header>
          <hr>



          <div class="jumbotron hero-spacer divInfoContract">
            <h3> Contract price: <?php echo $contractInfo[$i]["price"]; ?></h3>
                <form method="post" action="../controlador/ctrlContractsInfoAdmin.php" value="<?php echo $contractInfo[$i]["information"]; ?>">
                    <div class="row">
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="priceValue" placeholder="(If the value is 0, insert FREE)">
                        </div>
                        <div class="col-sm-5">
                            <button type="submit" name="priceSubmit" value="<?php echo $contractInfo[$i]["contractType"]; ?>" class="btn btn-default"> <span>Change </span></button>
                        </div>
                    </div>
                </form>
                <hr>

                <h3> Contract information: </h3>
                <h4><?php echo $contractInfo[$i]["contractDescription"]; ?> </h4>
                <form method="post" action="../controlador/ctrlContractsInfoAdmin.php">
                <div class="row">
                    <div class="col-sm-5">
                        <textarea class="form-control" name="infoValue"> <?php echo $contractInfo[$i]["information"]; ?></textarea>
                    </div>
                    <div class="col-sm-5">
                        <button type="submit" name="infoSubmit" value="<?php echo $contractInfo[$i]["contractType"]; ?>" class="btn btn-default"> <span>Change </span></button>
                    </div>
                </div>
                </form>

            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>


</body></html>