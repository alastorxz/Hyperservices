<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/dashProducts.css" rel="stylesheet">


  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
        <form method="post" action="../controlador/controladorAdmin.php">
                <div id="navbar" class="navbar-collapse collapse">
                   <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
                </div>
        </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
           <ul class="nav nav-sidebar">
               <li>
                  <form method="post" action="../controlador/controladorAdmin.php">
                     <button class="btn btn-primary dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                  </form>
               </li>
               <li>
                  <form method="post" action="../controlador/controladorAdmin.php">
                     <button class="btn btn-default contracts-link" type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                  </form>
               </li>
               <li>
                  <form method="post" action="../controlador/controladorAdmin.php">
                     <button class="btn btn-default contracts-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                  </form>
               </li>
               <li>
                  <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default contracts-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                  </form>
               </li>
           </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header">Admin Dashboard</h1>
                  <ul class="nav nav-pills">
                    <li class="active"><a href="#">Products</a></li>
                    <li>
                    <form method="post" action="../controlador/controladorAdmin.php">
                        <button type="submit" name="dashboardContractsAdmin" value="dashboardContractsAdmin" class="btn btn-default boton-link">Contracts</button>
                    </form>
                  </ul>

                  <div class="row placeholders">
                    <?php
                        for($i=0; $i<count($productos); $i++){
                    ?>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img src="../vista/images/products/<?php echo $productos[$i]["image"]?>" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4><?php echo $productos[$i]["productName"]; ?></h4><br>
                                <p><strong><?php echo $productos[$i]["price"]; ?>€</strong></p>
                                <div class="cortar">
                                    <span><?php  echo $productos[$i]["productDescription"];?></span>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                  </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body></html>