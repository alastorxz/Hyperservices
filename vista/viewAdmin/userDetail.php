<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <script src="../vista/js/jquery-2.1.3.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/admin/userDetail.css" rel="stylesheet">

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>

        <form method="post" action="../controlador/controladorAdmin.php">
        <div id="navbar" class="navbar-collapse collapse">
           <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
        </div>
        </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-primary users-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <?php for($i=0; $i<count($infoUser);$i++){ ?>
          <header class="jumbotron hero-spacer fotoUsuario">
            <div class="subirFoto">
                <img src="../vista/images/fotosUser/<?php echo $infoUser[$i]["image"];?>" class="img-responsive" alt="User Foto">
                <form method="post" action="../controlador/controladorUsersAdmin.php" enctype="multipart/form-data">
                    <fieldset class="form-group">
                        <label for="fotoUser">File input</label>
                        <input type="file" name="fotoUser" required>
                        <button class="btn btn-default uploadFoto" type="submit" name="loadFoto" value="loadFoto"><span>Upload Foto</span></button>
                        <input class="idOcult" type="text" name="userId" value="<?php echo $infoUser[$i]["id"]; ?>">
                    </fieldset>
                </form>
            </div>
            <div class="infoHeaderUsuario">
                <h2><?php echo $infoUser[$i]["firstName"]; echo " ".$infoUser[$i]["lastName"];?></h2>
                <p><?php echo $infoUser[$i]["nameUser"]; ?></p>
                <p>
                    <span id="city"><?php echo $infoUser[$i]["city"]; ?></span>
                    <span id="country"><?php echo ", ".$infoUser[$i]["country"]; ?>
                    <?php if($infoUser[$i]["businessSector"]){ ?>
                    <span id="businessSector"> | <?php echo $infoUser[$i]["businessSector"]; ?></span>
                    <?php } ?>
                </p>
            </div>
          </header>
          <hr>
          <div class="jumbotron hero-spacer divInfoUsuario">
            <div class="infoUser1">
                <?php foreach($infoUser as $user){
                    if($user["userType"] == "client"){ ?>
                        <form method="post" action="../controlador/controladorUsersAdmin.php">
                            <div class="togglebutton">
                              <label>
                                <span>User Type:</span>
                                    <?php echo $user["userType"]; ?>
                                    <input type="checkbox" required name="typeChecked" value="<?php echo $user["userType"]; ?>">
                                    <span class="toggle"></span>
                                    <button class="btn btn-default" type="submit" name="changeUserType" value="<?php echo $user["id"];?>">Change User Type</button>
                              </label>
                            </div>
                        </form>
                        <form method="post" action="../controlador/controladorUsersAdmin.php">
                            <div>
                              <label>
                                <span>Password:</span>
                                    <?php echo $user["passwd"]; ?>
                              </label>
                              <button class="btn btn-default" type="submit" name="changePasswd" value="<?php echo $user["id"]; ?>">
                                  <span class="glyphicon glyphicon-lock"></span>
                                  Change Password
                              </button>
                            </div>
                        </form>
               <?php }elseif ($user["userType"] == "admin") { ?>
                        <form method="post" action="../controlador/controladorUsersAdmin.php">
                            <div>
                              <label>
                                <span>Password:</span>
                                <?php
                                    foreach($infoUser as $user){
                                        echo $user["passwd"];
                                    }
                                ?>
                              </label>
                              <button class="btn btn-default" type="submit" name="changePasswd" value="<?php echo $user["id"]; ?>">
                                  <span class="glyphicon glyphicon-lock"></span>
                                  Change Password
                              </button>
                            </div>
                        </form>
                    <?php }
                } ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>


</body></html>