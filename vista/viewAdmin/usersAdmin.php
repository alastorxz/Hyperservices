<?php
    session_start();
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>HyperServices</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <!--CSS -->
        <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../vista/css/dashboard.css" rel="stylesheet">
        <link href="../vista/css/admin/usersAdmin.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                <?php
                    echo "Welcome ". $_SESSION["user"];
                ?>
              </a>
            </div>
          <form method="post" action="../controlador/controladorAdmin.php">
                  <div id="navbar" class="navbar-collapse collapse">
                     <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
                  </div>
                  </form>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li>
                    <form method="post" action="../controlador/controladorAdmin.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorAdmin.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorAdmin.php">
                        <button class="btn btn-default dashBoard-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                    </form>
                </li>
                <li>
                    <form method="post" action="../controlador/controladorAdmin.php">
                        <button class="btn btn-primary users-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                    </form>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Users Administration</h1>
            <div class="row placeholders">
                <div class="list-group">
                    <form method="post" action="../controlador/controladorUsersAdmin.php">
                        <?php foreach($infoUsers as $user){ ?>
                        <div class="list-group">
                            <button type="submit" name="id" class="list-group-item" value="<?php echo $user["id"]; ?>">
                            <h4 class="list-group-item-heading">
                                <span>Name:
                                    <span><?php echo $user["firstName"]; ?></span>
                                    <span><?php echo $user["lastName"]; ?></span>
                                </span>
                            </h4>
                            <p class="list-group-item-text">
                                <span>Username:
                                    <span><?php echo $user["nameUser"]; ?></span>
                                </span>
                            </p>
                            <p class="list-group-item-text">
                                <span>User Type:
                                    <span><?php echo $user["userType"]; ?></span>
                                </span>
                            </p>
                            <p class="list-group-item-text">
                                <span>Mail:
                                    <span><?php echo $user["mail"]; ?></span>
                                </span>
                            </p>
                            </button>
                        </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

</body></html>