<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/admin/casesAdmin.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>
        <form method="post" action="../controlador/controladorAdmin.php">
          <div id="navbar" class="navbar-collapse collapse">
             <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
          </div>
        </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
           <ul class="nav nav-sidebar">
               <li>
                   <form method="post" action="../controlador/controladorAdmin.php">
                       <button class="btn btn-default dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                    </form>
               </li>
                <li>
                   <form method="post" action="../controlador/controladorAdmin.php">
                       <button class="btn btn-default dashBoard-link " type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                   </form>
                </li>
                <li>
                   <form method="post" action="../controlador/controladorAdmin.php">
                       <button class="btn btn-primary cases-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                   </form>
                </li>
                <li>
                   <form method="post" action="../controlador/controladorAdmin.php">
                       <button class="btn btn-default dashBoard-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                   </form>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Cases</h1>
          <div class="row placeholders">
            <div class="list-group">
                <form method="post" action="../controlador/controladorIncidenciasAdmin.php">
             <?php
                for($i=0;$i<count($allCases);$i++){
             ?>
                <div class="list-group casesPosition">
                 <button type="submit" name="caseId" value=<?php echo $allCases[$i]["id"]?> class="list-group-item">
                     <h4 class="list-group-item-heading"><?php echo $allCases[$i]["caseTitle"]; ?></h4>
                     <p class="list-group-item-text marginElements divTextArea"><span>Case Description: </span><?php echo $allCases[$i]["caseDescription"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Status: </span><?php echo $allCases[$i]["caseStatus"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Urgency: </span><?php echo $allCases[$i]["caseUrgency"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Impact: </span><?php echo $allCases[$i]["caseImpact"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Case Resolution: </span><?php echo $allCases[$i]["caseResolution"]; ?></p>
                     <p class="list-group-item-text marginElements"><span>Related Contract: </span><?php echo $allCases[$i]["relatedContract"]; ?></p>
                 </button>
               </div>
             <?php
               }
             ?>

            </div>
            </form>

          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body></html>