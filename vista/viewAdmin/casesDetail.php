<?php
    session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>HyperServices</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <script src="../vista/js/jquery-2.1.3.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="../vista/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../vista/css/dashboard.css" rel="stylesheet">
    <link href="../vista/css/admin/casesDetail.css" rel="stylesheet">

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <?php
                echo "Welcome ". $_SESSION["user"];
            ?>
          </a>
        </div>

        <form method="post" action="../controlador/controladorAdmin.php">
        <div id="navbar" class="navbar-collapse collapse">
           <button type="submit" name="logOut" value="logOut" class="navbar-form navbar-right btn btn-info margins" href="../vista/principal.php">LOG OUT</button>
        </div>
        </form>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="dashboardAdmin" value="dashboardAdmin"><span>Dashboard<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="contractAdmin" value="contractAdmin"><span>Contract Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default cases-link" type="submit" name="caseAdmin" value="caseAdmin"><span>Cases Administration<span></button>
                </form>
            </li>
            <li>
                <form method="post" action="../controlador/controladorAdmin.php">
                    <button class="btn btn-default dashBoard-link" type="submit" name="userAdmin" value="userAdmin"><span>Users Administration<span></button>
                </form>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php foreach($infoCase as $field){ ?>
            <div class="jumbotron hero-spacer">
                <h2>CASE INFORMATION</h2>
                <p>Case ID:
                <span><?php echo $field["id"]; ?></span>
                </p>
                <p>
                Related Contract:
                <span><?php echo $field["relatedContract"]; ?></span>
                </p>
                <p>
                Related User:
                <span><?php echo $infoRelatedUser; ?></span>
                </p>
                <p>
                Related Product:
                <span><?php echo $infoRelatedProduct; ?></span>
                </p>
            </div>
            <hr>
            <div class="jumbotron hero-spacer">
                <h2>CASE EDITION</h2>
                <form method="post" action="../controlador/controladorIncidenciasAdmin.php">
                    <input type="text" class="ocult" name="caseIdOcult" value="<?php echo $field["id"]; ?>">
                    <section>
                        <p>Case Title:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="caseTitle" value="<?php echo $field["caseTitle"]; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseTitle" value="updateCaseTitle">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                        <p>Case Status:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                      <select class="form-control" name="caseStatus">
                                        <?php if($field["caseStatus"] == "Opened"){ ?>
                                            <option value="<?php echo $field["caseStatus"];?>"><?php echo $field["caseStatus"];?></option>
                                            <option value="In Development">In Development</option>
                                            <option value="Closed">Closed</option>
                                        <?php }elseif($field["caseStatus"] == "In Development") { ?>
                                            <option value="<?php echo $field["caseStatus"];?>"><?php echo $field["caseStatus"];?></option>
                                            <option value="Opened">Opened</option>
                                            <option value="Closed">Closed</option>
                                        <?php }elseif($field["caseStatus"] == "Closed"){ ?>
                                            <option value="<?php echo $field["caseStatus"];?>"><?php echo $field["caseStatus"];?></option>
                                            <option value="Opened">Opened</option>
                                            <option value="In Development">In Development</option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseStatus" value="updateCaseStatus">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                        <p>Case Description:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <?php if($field["caseDescription"]){ ?>
                                        <textarea name="caseDescription" class="form-control" rows="5"><?php echo $field["caseDescription"]; ?></textarea>
                                        <?php }else { ?>
                                        <textarea name="caseDescription" class="form-control" rows="5"></textarea>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseDescription" value="updateCaseDescription">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                    </section>
                    <section>
                        <p>Case Urgency:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" name="caseUrgency">
                                        <?php if($field["caseUrgency"] == "Low"){ ?>
                                            <option value="<?php echo $field["caseUrgency"];?>"><?php echo $field["caseUrgency"];?></option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                        <?php }elseif($field["caseUrgency"] == "Medium") { ?>
                                            <option value="<?php echo $field["caseUrgency"];?>"><?php echo $field["caseUrgency"];?></option>
                                            <option value="Low">Low</option>
                                            <option value="High">High</option>
                                        <?php }elseif($field["caseUrgency"] == "High"){ ?>
                                            <option value="<?php echo $field["caseUrgency"];?>"><?php echo $field["caseUrgency"];?></option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseUrgency" value="updateCaseUrgency">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                        <p>Case Impact:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" name="caseImpact">
                                        <?php if($field["caseImpact"] == "Low"){ ?>
                                            <option value="<?php echo $field["caseImpact"];?>"><?php echo $field["caseImpact"];?></option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                        <?php }elseif($field["caseImpact"] == "Medium") { ?>
                                            <option value="<?php echo $field["caseImpact"];?>"><?php echo $field["caseImpact"];?></option>
                                            <option value="Low">Low</option>
                                            <option value="High">High</option>
                                        <?php }elseif($field["caseImpact"] == "High"){ ?>
                                            <option value="<?php echo $field["caseImpact"];?>"><?php echo $field["caseImpact"];?></option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseImpact" value="updateCaseImpact">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                        <p>Case Resolution:</p>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <?php if($field["caseResolution"]){ ?>
                                        <textarea name="caseResolution" class="form-control" rows="5"><?php echo $field["caseResolution"]; ?></textarea>
                                        <?php }else { ?>
                                        <textarea name="caseResolution" class="form-control" rows="5"></textarea>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-default" type="submit" name="updateCaseResolution" value="updateCaseResolution">
                                        <span>Change</span>
                                    </button>
                                </div>
                            </div>
                    </section>
                </form>
            <?php }?>
            </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../vista/js/jquery-1.11.3.min.js"></script>


</body></html>