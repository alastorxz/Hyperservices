<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HyperServices</title>
	<link rel="stylesheet" type="text/css" href="../vista/css/principal.css">
</head>
<body>
	<div id="page">
		<div id="head">
			<div class="content">
				<h1 id="logo">
					<a href="../vista/principal.php">
						<img src="../vista/images/principal/LOGO.PNG" width="700">
					</a>
				</h1>
				<div id="subheader">
					<h2>HyperServices: Issue Tracking System</h2>
					<p>
						<strong>HyperServices</strong>
						It is a tool designed to support technical services in incident management. It is a web-based tool that comes oriented support departments, for control and monitoring of incidents that connect customers or users responsible.
					</p>
					<p>
						<strong>HyperServices</strong>
						is an application that
						<strong>no need to install anything on your computer</strong>
						, only requires a web browser and an Internet connection.
					</p>
				</div>
			</div>
		</div>
		<div id="menu">
			<ul>
				<li>
					<a href="../vista/principal.php">Home</a>
				</li>
				<li class="active">
				    <span>Products and Contracts</span>
				</li>
			</ul>
		</div>
		<div id="body">
			<div class="content">
				<div id="home">

					    <div class="info">
					        <h2>PRODUCTS</h2>
                    			<p>
                    			<strong>HyperServices</strong>
                    				has his own products for renting. We acquire our products from official sponsors like <strong>Dell</strong>, <strong>HP</strong>, and <strong> CISCO </strong>.
                    			</p>
                    			<p>
                    				With our portal, you can obtain a great variety of products, from switches to Insdustrial Printers. We ensure quality and the best prices over all our products.

                    			</p>
                    		    <p>
                    				The only thing you must do is to create a new Contract with us!
                    			</p>

                    			<BR>

                    		<h2>CONTRACTS</h2>
                                <p>
                                   To rent our products you must create a <strong>Contract</strong>. Contracts are your links with us, to ensure you our support services and make easy payments.

                                </p>
                                <p>
                                    You can acquire 3 types of contract, the <strong>No support </strong> contract, the <strong>Customer support </strong>, who ensures a 12x5 support to users,
                                    and the <strong> Premium Support </strong> contract, who provides a 24x7 support from our technicians, and support privileges.
                                </p>
                                <p>
                                    Earn your contract today with just one click!
                                </p>
                                <p>
                                JOIN <strong>HYPERSERVICES </strong>!
                                </p>
                    	</div>

				</div>
			</div>
		</div>
		<div id="foot">
			<div class="content">
				<div class="os box">
				    <a href="http://store.hp.com/">
				        <img class="providers" src="../vista/images/principal/HP-logo.png" alt="HP Store" title="Hp Store">
				    </a>
				    <a href="http://www.cisco.com/">
				        <img class="providers" src="../vista/images/principal/cisco.png" alt="Cisco Store" title="Cisco Store">
				    </a>
				    <a href="http://www.dell.com/">
                        <img class="providers" src="../vista/images/principal/Dell-icon.png" alt="Dell Store" title="Dell Store">
                    </a>
				</div>
				<div class="os box">
					<a href="http://www.microsoft.es/">
						<img src="../vista/images/principal/microsoft.gif" alt="Microsoft" title="Microsoft">
					</a>
					<a href="http://www.apple.com/es/">
						<img src="../vista/images/principal/apple.gif" alt="Apple" title="Apple">
					</a>
					<a href="http://www.gnu.org/home.es.html">
						<img src="../vista/images/principal/linux.gif" alt="GNU/Linux" title="GNU/Linux">
					</a>
				</div>
				<div class="navigators box">
					<a href="http://www.microsoft.com/spain/windows/products/winfamily/ie/default.mspx">
						<img src="../vista/images/principal/ie.gif" alt="Internet Explorer" title="Internet Explorer">
					</a>
					<a href="http://www.mozilla-europe.org/es/firefox/">
						<img src="../vista/images/principal/ff.gif" alt="Firefox" title="Firefox">
					</a>
					<a href="http://www.opera.com/">
						<img src="../vista/images/principal/opera.gif" alt="Opera" title="Opera">
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>