<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HyperServices</title>
    <link rel="stylesheet" href="../vista/css/registro.css" type="text/css">
    <script src="../vista/js/jquery-2.1.3.js"></script>
    <script src="../vista/js/registro.js"></script>

</head>
<body>
    <div class="new-webinar webinar">
        <section class="page-content">
            <div class="page-content--title">
                <h1 class="headline">
                How we work with
                <span class="break">Technical maintenance services</span>
                </h1>
            </div>
            <div class="page-content--blocks page-content--blocks">
                <p class="text-left">
                HyperServices has a sales service with highly trained staff and constant training. We have an internal laboratory with the necessary equipment to repair hardware (servers, computers, laptops, printers, peripherals, hard drives ...). 
Maintenance Service Systems is the star service HyperServices. This service is to assign a computer technician first class CUSTOMER to perform maintenance in all areas of technology CUSTOMER that this has reported one case of incidence.
The customer will have access to an Incident Manager where users can record their incidents and track them. In regular visits these incidents will be resolved, except those considered critical, in which case they will be addressed in a maximum period of 24 hours.
                </p>
                <p class="title">You can see our platform beta:</p>
                <div class="beta">
                    <img src="../vista/images/principal/registrobeta.PNG">
                </div
                <hr>
            </div>
        </section>
        <section class="sidebar-form">
            <div class="webinar-form">
                <div class="fsBody fsEmbed">
                    <form method="post" accept-charset="UTF-8" class="fsForm fsSingleColumn" action="../controlador/controladorRegistre.php">
                        <div class="fsPage">
                            <div class="fsSection fs1Col">
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <ul class="menu">
                                            <li>
                                                <label for="firstName" class="fsLabel fsRequiredLabel">First Name
                                                    <span class="fsRequiredMarker">*</span>
                                                </label>
                                                <input type="text" size="15" name="firstName" class="fsField fsRequired" value required>
                                            </li>
                                            <li>
                                                <label for="lastName" class="fsLabel fsRequiredLabel">Last Name
                                                    <span class="fsRequiredMarker">*</span>
                                                </label>
                                                <input type="text" size="15" name="lastName" class="fsField fsRequired" value required>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <ul class="menu">
                                            <li>
                                                <label for="usernameR" class="fsLabel fsRequiredLabel">Username
                                                    <span class="fsRequiredMarker">*</span>
                                                </label>
                                                <input type="text" size="15" id="usernameR" name="usernameR" class="fsField fsRequired" value required>
                                                <?php if($success == false){ ?>
                                                    <span class="alreadyExists" id="alreadyExists">Username already exists</span>
                                                <?php } ?>
                                            </li>
                                            <li>
                                                <label for="passwordR" class="fsLabel fsRequiredLabel">Password
                                                    <span class="fsRequiredMarker">*</span>
                                                </label>
                                                <input type="password" size="50" name="passwordR" class="fsField fsRequired" value required>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <label for="email" class="fsLabel fsRequiredLabel">Mail
                                            <span class="fsRequiredMarker">*</span>
                                        </label>
                                        <input type="email" size="50" name="email" class="fsField fsRequired" value required>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <label for="country" class="fsLabel fsRequiredLabel">Country
                                            <span class="fsRequiredMarker">*</span>
                                        </label>
                                        <input type="text" size="50" name="country" class="fsField fsRequired" value required>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <label for="city" class="fsLabel fsRequiredLabel">City
                                            <span class="fsRequiredMarker">*</span>
                                        </label>
                                        <input type="text" size="50" name="city" class="fsField fsRequired" value required>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <label for="direction" class="fsLabel fsRequiredLabel">Direction
                                            <span class="fsRequiredMarker">*</span>
                                        </label>
                                        <input type="text" size="50" name="direction" class="fsField fsRequired" value required>
                                    </div>
                                </div>
                                <div class="fsRow fsFieldRow fsLastRow">
                                    <div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
                                        <label for="phone" class="fsLabel fsRequiredLabel">Phone
                                            <span class="fsRequiredMarker">*</span>
                                        </label>
                                        <input type="text" size="50" name="phone" class="fsField fsRequired" value required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fsSubmit fsPagination">
                            <input type="submit" class="fsSubmitButton" value="Register Now" name="register">
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</body>
</html>