<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HyperServices</title>
	<link rel="stylesheet" type="text/css" href="../vista/css/principal.css">
</head>
<body>
	<div id="page">
		<div id="head">
			<div class="content">
				<h1 id="logo">
					<a href="../vista/principal.php">
						<img src="../vista/images/principal/LOGO.PNG" width="700">
					</a>
				</h1>
				<div id="subheader">
					<h2>HyperServices: Issue Tracking System</h2>
					<p>
						<strong>HyperServices</strong>
						It is a tool designed to support technical services in incident management. It is a web-based tool that comes oriented support departments, for control and monitoring of incidents that connect customers or users responsible.
					</p>
					<p>
						<strong>HyperServices</strong>
						is an application that
						<strong>no need to install anything on your computer</strong>
						, only requires a web browser and an Internet connection.
					</p>
				</div>
			</div>
		</div>
		<div id="menu">
			<ul>
				<li class="active">
					<span>Home</span>
				</li>
				<li>
					<a href="../vista/IncidentManagement.php">Products and Contracts</a>
				</li>
			</ul>
		</div>
		<div id="body">
			<div class="content">
				<div id="home">
					<div id="content">
						<div class="info">
							<h2>HyperServices: Issue Tracking System</h2>
							<p>
								<strong>HyperServices</strong>
								It reduces the administrative bureaucracy and allows full support and issue tracking customers. The tool records all your data along with the history Reported incidents. All in an accessible, powerful and friendly to use interface.
							</p>
							<p>
							The statistics and indicators offers
								<strong>HyperServices</strong>
							to assess at any time the status of the service and the time dedicated to the resolution of incidents.
							</p>
							<p>
							Increase the quality of your
								<acronym title="User assistance center">UAC</acronym>
								with
								<strong>HyperServices</strong>
							</p>
						</div>
					</div>
					<div id="login" class="infobox">
						<form method="post" accept-charset="UTF-8" action="../controlador/controladorPortal.php" class="fsForm fsSingleColumn">
							<div class="fsPage">
								<div class="fsSection fs1Col">
									<div class="fsRow fsFieldRow fsLastRow">
										<div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
											<label for="username" class="fsLabel fsRequiredLabel">Username
												<span class="fsRequiredMarker">*</span>
											</label>
											<input type="text" size="15" name="username" class="fsField fsRequired" value required>
										</div>
									</div>
									<div class="fsRow fsFieldRow fsLastRow">
										<div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100">
											<label for="password" class="fsLabel fsRequiredLabel">Password
												<span class="fsRequiredMarker">*</span>
											</label>
											<input type="password" size="15" name="password" class="fsField fsRequired" value required>
										</div>
									</div>
								</div>
							</div>
							<div class="fsSubmit fsPagination">
								<input type="submit" class="fsSubmitButton" value="Sign In" name="login">
							</div>
						</form>
						<div>
                            <p class="linkRegistro">Not yet Registered?
                                <form class="linkRegistroStForm" method="post" action="../controlador/controladorPortal.php">
                                    <button type="submit" value="register" name="register" class="linkRegistroSt">
                                        Register Now!
                                    </button>
                                </form>
                            </p>
                        </div>
					</div>
					<div class="imagebox">
					    <img src="../vista/images/principal/IBM_RTP_DATA_CENTER_2.jpg" height="280" width="350">
					</div>
				</div>
			</div>
		</div>
		<div id="foot">
			<div class="content">
				<div class="os box">
				    <a href="http://store.hp.com/">
				        <img class="providers" src="../vista/images/principal/HP-logo.png" alt="HP Store" title="Hp Store">
				    </a>
				    <a href="http://www.cisco.com/">
				        <img class="providers" src="../vista/images/principal/cisco.png" alt="Cisco Store" title="Cisco Store">
				    </a>
				    <a href="http://www.dell.com/">
                        <img class="providers" src="../vista/images/principal/Dell-icon.png" alt="Dell Store" title="Dell Store">
                    </a>
				</div>
				<div class="os box">
					<a href="http://www.microsoft.es/">
						<img src="../vista/images/principal/microsoft.gif" alt="Microsoft" title="Microsoft">
					</a>
					<a href="http://www.apple.com/es/">
						<img src="../vista/images/principal/apple.gif" alt="Apple" title="Apple">
					</a>
					<a href="http://www.gnu.org/home.es.html">
						<img src="../vista/images/principal/linux.gif" alt="GNU/Linux" title="GNU/Linux">
					</a>
				</div>
				<div class="navigators box">
					<a href="http://www.microsoft.com/spain/windows/products/winfamily/ie/default.mspx">
						<img src="../vista/images/principal/ie.gif" alt="Internet Explorer" title="Internet Explorer">
					</a>
					<a href="http://www.mozilla-europe.org/es/firefox/">
						<img src="../vista/images/principal/ff.gif" alt="Firefox" title="Firefox">
					</a>
					<a href="http://www.opera.com/">
						<img src="../vista/images/principal/opera.gif" alt="Opera" title="Opera">
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>